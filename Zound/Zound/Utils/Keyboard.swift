//
//  Keyboard.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 02/03/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

public typealias KeyboardAnimationTuple = (frame: CGRect, animationDuration: TimeInterval, animationCurve: Int)

public class Keyboard {

    public static func keyboardFrameChangeAnimationInfoObservable() -> Observable<KeyboardAnimationTuple> {
        
        let keyboardWillShowUserInfo = NotificationCenter.default.rx.notification(NSNotification.Name.UIKeyboardWillShow).map{ $0.userInfo}
        let keyboardWillHideUserInfo = NotificationCenter.default.rx.notification(NSNotification.Name.UIKeyboardWillHide).map{ $0.userInfo}
        let keyboardFrameChangedAnimationInfo: Observable<KeyboardAnimationTuple> = Observable.of(keyboardWillShowUserInfo, keyboardWillHideUserInfo)
            
            .merge().map{ userInfo in
                
                let frame = (userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
                let animationDuration = (userInfo![UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
                let animateCurve = (userInfo![UIKeyboardAnimationCurveUserInfoKey]! as AnyObject).integerValue!
                
                return (frame: frame, animationDuration:animationDuration, animationCurve: animateCurve)
        }
        return keyboardFrameChangedAnimationInfo
}
}

//
//  Zound.h
//  Zound
//
//  Created by Raul Andrisan on 04/05/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for Zound.
FOUNDATION_EXPORT double ZoundVersionNumber;

//! Project version string for Zound.
FOUNDATION_EXPORT const unsigned char ZoundVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Zound/PublicHeader.h>


#import "Zound-Bridging-Header.h"

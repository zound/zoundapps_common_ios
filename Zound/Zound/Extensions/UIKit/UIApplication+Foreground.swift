//
//  UIApplication+Foreground.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 27/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

extension UIApplication {
    
    public static var appIsInForeground: Observable<Bool> {
        
        let willEnterForeground = NotificationCenter.default.rx.notification(NSNotification.Name.UIApplicationWillEnterForeground).replaceWith(true)
        let didEnterBackground = NotificationCenter.default.rx.notification(NSNotification.Name.UIApplicationDidEnterBackground).replaceWith(false)
        let applicationIsCurrentlyActive = UIApplication.shared.applicationState != .background
        let appIsInForegroud = Observable.of(willEnterForeground, didEnterBackground).merge().startWith(applicationIsCurrentlyActive)
        return appIsInForegroud
    }
}

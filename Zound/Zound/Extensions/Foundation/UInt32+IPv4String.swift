//
//  UInt32+IPv4String.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 21/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

extension UInt32 {
    
    public func IPv4String() -> String {
        
        let ip = self
        
        let byte1 = UInt8(ip & 0xff)
        let byte2 = UInt8((ip>>8) & 0xff)
        let byte3 = UInt8((ip>>16) & 0xff)
        let byte4 = UInt8((ip>>24) & 0xff)
        
        return "\(byte1).\(byte2).\(byte3).\(byte4)"
    }
}
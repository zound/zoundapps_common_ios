//
//  SelectableViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 06/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxSwift

class SelectableViewController: UIViewController {
    
    var relativeDistanceFromCenter : Double
    let viewModelVariable: Variable<SelectableViewModel?> = Variable(nil)
    var viewModel: SelectableViewModel? {
        get { return viewModelVariable.value }
        set { viewModelVariable.value = newValue }
    }
    
    var nextFillsScreen: Bool
    var prevFillsScreen: Bool
    
    required init?(coder aDecoder: NSCoder) {
        
        relativeDistanceFromCenter = 0;
        nextFillsScreen = false
        prevFillsScreen = false
        super.init(coder: aDecoder)
    }
    
    
}

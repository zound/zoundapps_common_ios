//
//  SettingsAbout.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import MinuetSDK

protocol SettingsAboutViewControllerDelegate: class {
    
    func settingsAboutViewControllerDidRequestBack(_ viewController: SettingsAboutViewController)
    func settingsAboutViewControllerDidRequestUpdate(_ viewController: SettingsAboutViewController, forSpeaker speaker: Speaker)
}

class SettingsAboutViewController: UIViewController {
    
    weak var delegate: SettingsAboutViewControllerDelegate?
    var viewModel: SettingsAboutViewModel?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var aboutLabel: UILabel!
    @IBOutlet weak var updateButton: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var topSeparator: UIView!
    @IBOutlet weak var topSeparatorHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.Settings.About.Title.uppercased())
        aboutLabel.font = Fonts.UrbanEars.Light(15)
        scrollView.delegate = self
        scrollView.alwaysBounceVertical = true
        
        self.topSeparatorHeightConstraint.constant = 0.5
        
        self.updateButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Settings.About.Update.Buttonlabel), for: .normal)
        
        if self.viewModel?.updateAvailable == true {
            self.updateButton.isHidden = false
        } else {
            self.updateButton.isHidden = true
        }
        
        updateTopSeparatorAlpha()
        
        if let vm = viewModel {
            Observable.combineLatest(vm.infoDictVariable.asObservable(),
                                     vm.wifiSignal.asObservable()
                )
            { (infoDict, wifiSignal) in
                
                let wifiNameLabel = Localizations.Settings.About.WiFiNetwork
                let ipLabel = Localizations.Settings.About.Ip
                let nameLabel = Localizations.Settings.About.Name
                let macLabel = Localizations.Settings.About.Mac
                let modelLabel = Localizations.Settings.About.Model
                let fwVersionLabel = Localizations.Settings.About.BuildVersion
                let castVersionLabel = Localizations.Settings.About.CastVersion
                
                
                let labels = [nameLabel, modelLabel, wifiNameLabel, ipLabel,
                              macLabel, fwVersionLabel, castVersionLabel].map {
                                NSAttributedString(string: $0, attributes: [NSForegroundColorAttributeName: UIColor.white])
                }
                
                let empty = ""
                
                let wifiName = infoDict[vm.wifiName_key] ?? empty
                let name = infoDict[vm.name_key] ?? empty
                let ip = infoDict[vm.ip_key] ?? empty
                let mac = infoDict[vm.mac_key]?.formatToMAC() ?? empty
                let model = infoDict[vm.model_key] ?? empty
                let buildVersion = infoDict[vm.fwVersion_key] ?? empty
                let castVersion = infoDict[vm.castVersion_key] ?? empty
                
                let values = [name, model, wifiName + " (\(wifiSignal)%)", ip, mac, buildVersion, castVersion].map {
                    NSAttributedString(string: $0, attributes: [NSForegroundColorAttributeName: UIColor("#CDCDCD")])
                }
                
                let spacer = NSAttributedString(string: " ")
                let newLine = NSAttributedString(string: "\n")
                let finalString = NSMutableAttributedString()
                
                for (label, value) in zip(labels, values) {
                    finalString.append(label)
                    finalString.append(spacer)
                    finalString.append(value)
                    finalString.append(newLine)
                }
                
                return finalString
                }.bind(to:aboutLabel.rx.attributedText).disposed(by: rx_disposeBag)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }

    
    @IBAction func onBack(_ sender: AnyObject) {
        
        self.delegate?.settingsAboutViewControllerDidRequestBack(self)
    }

    @IBAction func onUpdate(_ sender: AnyObject) {
        guard let speaker = viewModel?.speaker else { return }
        self.delegate?.settingsAboutViewControllerDidRequestUpdate(self, forSpeaker: speaker)
    }
    
    func updateTopSeparatorAlpha() {
        
        topSeparator.alpha = max(0,min(1.0,scrollView.contentOffset.y/22.0))
    }
}

extension SettingsAboutViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateTopSeparatorAlpha()
    }
}

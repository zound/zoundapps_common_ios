//
//  AboutViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 09/09/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

protocol AboutViewControllerDelegate: class {
    
    func aboutViewControllerDidRequestBack(_ aboutViewController: AboutViewController)
    func aboutViewControllerDidRequestAboutItem(_ helpItem: AboutItem, _ aboutViewController: AboutViewController)
}

enum AboutItem {
    
    case endUserLicense
    case openSource
    case privacyPolicy
    
    var localizedDisplayName: String {
        switch  self {
        case .endUserLicense: return Localizations.About.MenuItem.Eula
        case .openSource: return Localizations.About.MenuItem.Foss
        case .privacyPolicy: return Localizations.About.PrivacyPolicy.Title
        }
    }
}

class AboutViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableTopSeparatorHeight: NSLayoutConstraint!
    @IBOutlet weak var tableBottomSeparatorHeight: NSLayoutConstraint!
    
    
    
    weak var delegate: AboutViewControllerDelegate?
    
    @IBAction func onBack(_ sender: AnyObject) {
        
        self.delegate?.aboutViewControllerDidRequestBack(self)
    }
    
    let menuItems: [AboutItem]
    required init?(coder aDecoder: NSCoder) {
        self.menuItems = [.endUserLicense, .openSource, .privacyPolicy]
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.About.Title.uppercased())
        self.tableTopSeparatorHeight.constant = 0.5
        self.tableBottomSeparatorHeight.constant = 0.5
        versionLabel.font = Fonts.UrbanEars.Regular(15)
        if let version = Bundle.main.releaseVersionNumber, let build = Bundle.main.buildVersionNumber {
            versionLabel.text = Localizations.About.Version(version, build)
        }
        
        self.tableView.dataSource = self
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.delegate = self
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
}



extension AboutViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let menuItem = menuItems[(indexPath as NSIndexPath).row]
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "aboutItem", for: indexPath) as? AboutCell {
            cell.aboutItem = menuItem
            return cell
        }
        
        return UITableViewCell()
    }
    
}
extension AboutViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let setting = menuItems[(indexPath as NSIndexPath).row]
        self.delegate?.aboutViewControllerDidRequestAboutItem(setting, self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

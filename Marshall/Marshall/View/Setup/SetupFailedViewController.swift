//
//  SetupFailedViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 31/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import Cartography

protocol SetupFailedViewControllerDelegate: class {
    
    func setupFailedDidFinishTroubleshooting(_ viewcontroller:SetupFailedViewController)
    func setupFailedDidRequestCancelSetup(_ viewcontroller:SetupFailedViewController)
}

class SetupFailedViewController: UIViewController {
    

    @IBOutlet weak var pageContainerView: UIView!
    var viewModel: SetupNetworkViewModel?
    weak var delegate: SetupFailedViewControllerDelegate?
    @IBOutlet weak var titleLabel: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.Setup.Failed.Title)
        showFailedConnect()
        
        
    }
    
    func showFailedConnect() {
        let failedConnectViewController = UIStoryboard.setup.setupFailedConnectViewController
        failedConnectViewController.delegate = self
        transitionToViewController(failedConnectViewController)
    }
    
    func showFailedBoot() {
        let failedBootViewController = UIStoryboard.setup.setupFailedBootViewController
        failedBootViewController.delegate = self
        transitionToViewController(failedBootViewController)
    }
    
    func showFailedSetup() {
        let failedSetupViewController = UIStoryboard.setup.setupFailedSetupViewController
        failedSetupViewController.delegate = self
        transitionToViewController(failedSetupViewController)
    }
    
    func showFailedReset() {
        let failedResetViewController = UIStoryboard.setup.setupFailedResetViewController
        failedResetViewController.delegate = self
        transitionToViewController(failedResetViewController)
    }
    
    func transitionToViewController(_ viewController: UIViewController) {
        
        if self.childViewControllers.count == 0 {
            
            self.addChildViewController(viewController)
            pageContainerView.addSubview(viewController.view)
                                    constrain(viewController.view) { view in
                                        if let superview = view.superview {
                                            view.edges == superview.edges
                                        }
                                    }
            viewController.didMove(toParentViewController: self)
        } else {
            
            if let currentViewController = self.childViewControllers.first {
                
                self.addChildViewController(viewController)
                currentViewController.willMove(toParentViewController: nil)
                self.transition(from: currentViewController,
                                                  to: viewController,
                                                  duration: 0.15,
                                                  options: [.beginFromCurrentState, .transitionCrossDissolve],
                                                  animations: {
                                                    viewController.view.translatesAutoresizingMaskIntoConstraints = false;
                                                    constrain(viewController.view) { view in
                                                        if let superview = view.superview {
                                                            view.edges == superview.edges
                                                        }
                                                    }
                    },
                                                  completion: { finished in
                                                    currentViewController.removeFromParentViewController()
                                                    viewController.didMove(toParentViewController: self)
                                                    
                })
            }
        }
    }
    
    @IBAction func onCancel(_ sender: AnyObject) {
        cancelSetup()
    }
    
    

    func cancelSetup() {
        self.delegate?.setupFailedDidRequestCancelSetup(self)
    }
    func didFinishSetup() {
        self.delegate?.setupFailedDidFinishTroubleshooting(self)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
}



extension SetupFailedViewController: SetupFailedConnectViewControllerDelegate {
    
    func setupFailedConnectDidRequestNext(_ viewController: SetupFailedConnectViewController) {
        
        showFailedBoot()
    }
    
    func setupFailedConnectDidRequestCancel(_ viewController: SetupFailedConnectViewController) {
        
        cancelSetup()
    }
}

extension SetupFailedViewController: SetupFailedBootViewControllerDelegate {
    
    func setupFailedBootDidRequestNext(_ viewController: SetupFailedBootViewController) {
        
        showFailedSetup()
    }
    func setupFailedBootDidRequestCancel(_ viewController: SetupFailedBootViewController) {
        
        cancelSetup()
    }
}

extension SetupFailedViewController: SetupFailedSetupViewControllerDelegate {
    
    func setupFailedSetupDidConfirmSetupMode(_ viewController: SetupFailedSetupViewController) {
        
        didFinishSetup()
    }
    
    func setupFailedSetupDidDenySetupMode(_ viewController: SetupFailedSetupViewController) {
        
        showFailedReset()
    }
}

extension SetupFailedViewController: SetupFailedResetViewControllerDelegate {
    
    func setupFailedResetDidRequestNext(_ viewController: SetupFailedResetViewController) {
        
        showFailedSetup()
    }
    
    func setupFailedResetDidRequestCancel(_ viewController: SetupFailedResetViewController) {
     
        cancelSetup()
    }
}


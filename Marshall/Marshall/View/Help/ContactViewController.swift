//
//  ContactViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/02/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

protocol ContactViewControllerDelegate: class {
    func contactViewControllerDidRequestBack(_ contactViewController: ContactViewController)
    func contactViewControllerDidRequestGotoWebsite(_ website: String,  contactViewController: ContactViewController)
    func contactViewControllerDidRequestGotoSupportWebsite(_ supportWebsite : String,  contactViewController: ContactViewController)
}

class ContactViewController: UIViewController {
    weak var delegate: ContactViewControllerDelegate?
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var contentLabel: UILabel!
    @IBOutlet var sendEmailButton: UIButton!
    @IBOutlet var goToWebsiteButton: UIButton!
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.Help.Contact.Title.uppercased())
        
        contentLabel.text = Localizations.Help.Contact.Content
        contentLabel.font = Fonts.MainContentFont
        
        UIView.setAnimationsEnabled(false)
        
        goToWebsiteButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Appwide.GoToWebsite), for: .normal)
        sendEmailButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Help.Contact.Button.SendEmail), for: .normal)
        
        sendEmailButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
    }
}

// MARK: - ContactViewControllerDelegate
extension ContactViewController {
    @IBAction func onGotoWebsite(_ sender: Any) {
        self.delegate?.contactViewControllerDidRequestGotoWebsite(Localizations.Help.Contact.Website, contactViewController: self)
    }
    
    @IBAction func onSendEmail(_ sender: Any) {
        self.delegate?.contactViewControllerDidRequestGotoSupportWebsite(Localizations.Help.Contact.SupportWebsite, contactViewController: self)
    }

    @IBAction func onBack(_ sender: Any) {
        self.delegate?.contactViewControllerDidRequestBack(self)
    }
}

//
//  SettingsSpeakerListViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import MinuetSDK

class SettingsSpeakerListViewModel {
    
    let audioSystem: AudioSystem
    var cells: Variable<[SettingsSpeakerListCell]> = Variable([])
    var disposeBag = DisposeBag()
    
    
    init(audioSystem: AudioSystem) {
    
        self.audioSystem = audioSystem
        self.audioSystem.speakers.asObservable().subscribe(onNext: { [weak self] speakers in
            self?.buildCells()
        }).disposed(by: disposeBag)
    }

    
    func buildCells() {
        
        cells.value.removeAll()
        var builtCells: [SettingsSpeakerListCell] = []
        
        audioSystem.speakers.value.sorted(by: { speaker1, speaker2 in
            return speaker1.sortName.lowercased() < speaker2.sortName.lowercased()
        }).filter({ (speaker : Speaker) -> Bool in
            if let isConnectable = ExternalConfig.sharedInstance.speakerImageForColor(speaker.color)?.connectable {
                return isConnectable
            } else {
                return false
            }
        }).forEach({ speaker in
            let speakerCell = SettingsSpeakerCell()
            speakerCell.viewModel = speaker
            builtCells.append(SettingsSpeakerListCell.speakerCell(speakerCell))
        })
        
        //let streamingQualityCell = SettingsStreamingQualityCell()
        //builtCells.append(.streamingQualityCell(streamingQualityCell))
        
        cells.value = builtCells
    }
}

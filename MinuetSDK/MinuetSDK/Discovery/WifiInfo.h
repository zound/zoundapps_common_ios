//
//  WifiInfo.h
//  MinuetSDK
//
//  Created by Raul Andrisan on 26/04/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

#ifndef WifiInfo_h
#define WifiInfo_h


@interface WifiInfo : NSObject
+ (NSString*)wifiAddress;
@end

#endif /* WifiInfo_h */

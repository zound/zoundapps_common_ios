     //
//  UnconfiguredSpeaker.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 15/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public class UnconfiguredSpeaker: NSObject {
    
    public override init() {
        
        model = ""
        ssid = ""
        wacMac = "unknown"
        _nameIndex = 0
        super.init()
    }
    
    public init(model: String, ssid: String, wacMac: String) {
        
        self.model = model
        self.ssid = ssid
        self.wacMac = wacMac
        _nameIndex = 0
        
        super.init()
        
        SpeakerNameMacStore.sharedInstance.store(mac: mac, forName: nameIndexKey)
        _nameIndex = SpeakerNameMacStore.sharedInstance.index(ofMac: mac, forName: nameIndexKey) ?? 0
    }
    
    public let model : String
    public let ssid : String
    public let wacMac : String
    private var _nameIndex: Int
    public var nameIndex: Int {
        return _nameIndex
    }
    
    public var nameIndexKey: String {
     
        if let lastUnderscore = ssid.range(of: "_", options: .backwards, range: nil, locale: nil) {
            let substring = ssid[ssid.startIndex..<lastUnderscore.lowerBound]
            return substring
        } else {
            return ssid
        }
    }
    

    
    public var veniceMac: String {
            
            let components = wacMac.split(separator: ":").map{ String($0) }
        guard components.count==6 else {
            return ""
        }
            var hexWithLeadingZeroComponents = [String]()
            for (index,component) in components.enumerated() {
                let isLastComponent = index == components.count-1
                let intValue = Int(component, radix: 16)! - (isLastComponent ? 2 : 0)
                let hexString = String(format: "%02x", intValue)
                hexWithLeadingZeroComponents.append(hexString)
            }
            return hexWithLeadingZeroComponents.joined(separator: "").uppercased()
    }
    
    public var mac: String {
        
        let components = wacMac.split(separator: ":").map{ String($0) }
        guard components.count==6 else {
            return ""
        }
        var hexWithLeadingZeroComponents = [String]()
        for component in components {
            let intValue = Int(component, radix: 16)!
            let hexString = String(format: "%02x", intValue)
            hexWithLeadingZeroComponents.append(hexString)
        }
        return hexWithLeadingZeroComponents.joined(separator: "").uppercased()
    }
    
    public var macSegmentFromSSID: String? {
        
        if let lastDot = ssid.range(of: ".d", options: .backwards, range: nil, locale: nil) {
            
            let nameAndMac = ssid[ssid.startIndex..<lastDot.lowerBound]
            if let lastSpace = nameAndMac.range(of: " ", options: .backwards, range: nil, locale: nil) {
                if let macWithBrackets = String(nameAndMac[lastSpace.lowerBound..<nameAndMac.endIndex]) {
                    let mac = macWithBrackets.replacingOccurrences(of: "]", with: "").replacingOccurrences(of: "[", with: "").replacingOccurrences(of: ":", with: "").replacingOccurrences(of: " ", with: "")
                    return mac.uppercased()
                }
            } else {
                
                let last4Letters = nameAndMac.substring(from: nameAndMac.index(nameAndMac.endIndex, offsetBy: -4))
                let hexChars = CharacterSet(charactersIn: "abcdef0123456789")

                if last4Letters.count == 4 && last4Letters.rangeOfCharacter(from: hexChars.inverted) == nil {
                    return last4Letters.uppercased()
                }
            }
        }
        return nil
    }
}

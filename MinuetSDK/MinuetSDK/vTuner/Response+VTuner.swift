//
//  Response+VTuner.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 10/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import RxSwift
import Moya
import SWXMLHash

extension ObservableType where E == Response {
    
    func mapVTunerResponse<T: XMLIndexerDeserializable>() -> Observable<T> {
        return flatMap { response -> Observable<T> in
            
            return Observable.just(try response.mapVTunerResponse())
        }
        
    }
}

extension Response {
    /// Maps data received from the signal into a JSON object.
    func mapVTunerResponse<T: XMLIndexerDeserializable>() throws -> T {
        if(statusCode == 200) {
            
            //let string = NSString(data: data, encoding: NSUTF8StringEncoding)
            //NSLog(string)
            let xml = SWXMLHash.config { config in
                config.shouldProcessNamespaces = false
                config.shouldProcessLazily = false
                }.parse(data)
            
            if let response = try? xml.byKey("EncryptedToken") {
                
                let encryptedToken : T = try response.value()
                return encryptedToken
            }
            
            if let response = try? xml.byKey("ListOfItems") {
                
                let listResponse : T = try response.value()
                return listResponse
            }
            
            throw VTunerResponseError.VTunerErrorParse
            
        } else if (statusCode == 403) {
            
            throw VTunerResponseError.VTunerError403
        }  else if (statusCode == 404) {
            
           throw VTunerResponseError.VTunerError404
        }
        
        throw VTunerResponseError.VTunerErrorUnknown
    }
}

enum VTunerResponseError: Error {
    
    case vTunerError403
    case vTunerError404
    case vTunerError500
    case vTunerErrorUnknown
    case vTunerErrorParse
}

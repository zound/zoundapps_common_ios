//
//  GeoIPProvider.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 06/09/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import Moya
import RxSwift
import Moya_ObjectMapper

public class GeoIPProvider {
    
    var provider: RxMoyaProvider<GeoIPService>
    public init() {
        
        let geoIPProvider = RxMoyaProvider<GeoIPService>()
        provider = geoIPProvider
    }
    
    public func currentIPAddressLocation() -> Observable<GeoIPLocation> {
        
        return provider.request(GeoIPService.getLocation).mapObject(GeoIPLocation.self).asObservable()
    }
}

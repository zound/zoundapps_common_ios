//
//  SpotifyAuthProviderProtocol.swift
//  MinuetSDK
//
//  Created by Claudiu Alin Luminosu on 14/11/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift

protocol SpotifyAuthProvider {
    func swap(_ code: String, redirectUri: String) -> Observable<SpotifyToken>
    func refresh(_ token: SpotifyToken) -> Observable<SpotifyToken>
}

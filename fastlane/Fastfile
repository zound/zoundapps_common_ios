# Customise this file, documentation can be found here:
# https://github.com/fastlane/fastlane/tree/master/fastlane/docs
# All available actions: https://docs.fastlane.tools/actions
# can also be listed using the `fastlane actions` command

# Change the syntax highlighting to Ruby
# All lines starting with a # are ignored when running `fastlane`

# If you want to automatically update fastlane if a new version is available:
# update_fastlane

# This is the minimum version number required.
# Update this, if you use features of a newer version
fastlane_version "2.18.2"

default_platform :ios

platform :ios do

  desc "Change version and build number. Options: xcodeproj, build_number, version_number"
  lane :change_version do |options|
    increment_build_number(
      xcodeproj: options[:xcodeproj],
      build_number: options[:build_number]
    )
    increment_version_number(
      xcodeproj: options[:xcodeproj],
      version_number: options[:version_number]
    )
  end

  desc "Change version and build application. Options: app_name, build_number, version_number, configuration, export_method, build_as"
  lane :change_version_and_build do |options|
	v_xcodeproj = options[:app_name]+'/'+options[:app_name]+'.xcodeproj'
	v_plist = options[:app_name]+'/Info.plist'
	if options[:build_as] == "Tieto"
	  v_teamid = "7BPBY6BPNP"
	  if options[:app_name] == "UrbanEars"
	    v_app_identifier = "com.tieto.connect"
	  end
	  if options[:app_name] == "Marshall"
	    v_app_identifier = "com.tieto.marshall"
	  end
    end
	if options[:build_as] == "Zound"
	  v_teamid = "ESVC7L2F72"
	  if options[:app_name] == "UrbanEars"
	    v_app_identifier = "com.zoundindustries.connect"
	  end
	  if options[:app_name] == "Marshall"
	    v_app_identifier = "com.zoundindustries.marshall"
	  end
    end
	
    change_version(
      xcodeproj: v_xcodeproj,
      build_number: options[:build_number],
      version_number: options[:version_number]
    )
    update_app_identifier(
      xcodeproj: v_xcodeproj,
      plist_path: v_plist, # Path to info plist file, relative to xcodeproj
      app_identifier: v_app_identifier
    )
    update_project_team(
      path: v_xcodeproj,
      teamid: v_teamid
    )
    gym(
      workspace: "Zound.xcworkspace",
      scheme: options[:app_name],
      configuration: options[:configuration],
      xcargs: "-allowProvisioningUpdates",
      export_method: options[:export_method]
    )
  end

  desc "Upload build to iTunes Connect for TestFlight beta testing. Options: package_path, changelog, groups"
  lane :upload_build do |options|
    ENV["DELIVER_ITMSTRANSPORTER_ADDITIONAL_UPLOAD_PARAMETERS"] = "-t DAV" #needed to solve firewall issues
    if options[:groups].nil?
      pilot(
        username: "zoundapps.tieto@gmail.com",
        ipa: options[:package_path],
        changelog: options[:changelog],
        wait_for_uploaded_build: true
      )
    else
      pilot(
        username: "zoundapps.tieto@gmail.com",
        ipa: options[:package_path],
        changelog: options[:changelog],
        groups: options[:groups],
        wait_for_uploaded_build: true
      )
    end	  
  end
end


# More information about multiple platforms in fastlane: https://github.com/fastlane/fastlane/blob/master/fastlane/docs/Platforms.md
# All available actions: https://docs.fastlane.tools/actions

# fastlane reports which actions are used
# No personal data is recorded. Learn more at https://github.com/fastlane/enhancer

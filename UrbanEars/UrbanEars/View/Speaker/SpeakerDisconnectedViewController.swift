//
//  SpeakerDisconnectedViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 21/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import MinuetSDK

protocol SpeakerDisconnectedViewControllerDelegate: class {
    
    func speakerDisconnectedDidRequestReconnect(_ viewController: SpeakerDisconnectedViewController)
    func speakerDisconnectedDidRequestDisconnect(_ viewController: SpeakerDisconnectedViewController)
}

class SpeakerDisconnectedViewController: UIViewController {

    var viewModel: SpeakerViewModel?
    var delegate: SpeakerDisconnectedViewControllerDelegate?
    @IBOutlet weak var speakerImageView: UIImageView!
    @IBOutlet weak var disconnectedTextLabel: UILabel!
    @IBOutlet weak var reconnectButton: UIButton!
    @IBOutlet weak var homescreenButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIView.setAnimationsEnabled(false)
        reconnectButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Player.SessionLost.Buttons.Reconnect), for: .normal)
        homescreenButton.setAttributedTitle(Fonts.ButtonFont.AttributedSecondaryButtonWithString(Localizations.Appwide.Back), for: .normal)
        reconnectButton.layoutIfNeeded()
        homescreenButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        
        disconnectedTextLabel.font = Fonts.MainContentFont
        
        if let vm = viewModel {
            vm.connectionStatus.asObservable().subscribe(weak: self, onNext: SpeakerDisconnectedViewController.updateForConnectionStatus).disposed(by: rx_disposeBag)
            updateWithSpeaker(vm.clientSpeaker)
            
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    func updateWithSpeaker(_ speaker: Speaker) {
        
        if let speakerName = ExternalConfig.sharedInstance.speakerImageForColor(speaker.color)?.heroImageName {
            speakerImageView.image = UIImage(named: speakerName)
        } else {
            speakerImageView.image = UIImage(named: "hero_placeholder")
        }
    }
    
    func messageForSpeakerNotifierError(_ error: SpeakerNotifierError?) -> String? {
        
        if let friendlyName = viewModel?.clientSpeaker.friendlyName {
            if let notifierError = error {
                      
                switch  notifierError {
                case .networkConnectionLost:
                    return Localizations.Player.SessionLost.NetworkConnectionLostToSpeaker(friendlyName)
                case .sessionLost:
                    return Localizations.Player.SessionLost.UserIsControllingSpeaker(friendlyName)
                case .unknownError:
                    return Localizations.Player.SessionLost.GenericErrorToSpeaker(friendlyName)
                case .canceledByUser:
                    return Localizations.Player.SessionLost.GenericErrorToSpeaker(friendlyName)
                case .timeout:
                    return Localizations.Player.SessionLost.Timeout(friendlyName)
                case .paused:
                    return nil
                }
            } else {
                return  Localizations.Player.SessionLost.SpeakerNotConnected
            }
        }
        return nil
    }
    
    func updateForConnectionStatus(_ status: SpeakerConnectionStatus) {
        
        var message: String? = nil
        switch status {
        case .disconnected(let error):
            message = messageForSpeakerNotifierError(error)
        default: break
        }
        disconnectedTextLabel.text = message
    }
    @IBAction func onGotoHomescreen(_ sender: AnyObject) {
        
        self.delegate?.speakerDisconnectedDidRequestDisconnect(self)
    }
    @IBAction func onReconnect(_ sender: AnyObject) {
        
        self.delegate?.speakerDisconnectedDidRequestReconnect(self)
    }
}

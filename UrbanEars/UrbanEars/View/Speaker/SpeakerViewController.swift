//
//  SpeakerViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 14/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import Cartography
import RxCocoa
import RxSwift
import MarqueeLabel
import MinuetSDK
import Zound

protocol SpeakerViewControllerDelegate: class {
    
    func speakerViewControllerDidRequestBack(_ speakerViewController: SpeakerViewController)
    
    func speakerViewControllerDidRequestSpotifyConnect(_ speakerViewController: SpeakerViewController)
    func speakerViewControllerDidRequestGoogleCast(_ speakerViewController: SpeakerViewController)
    func speakerViewControllerDidRequestAirplay(_ speakerViewController: SpeakerViewController)
    func speakerViewControllerDidRequestPresetDisabledInfo(_ speakerViewController: SpeakerViewController)
    
    func viewControllerDidRequestVolume(_ viewController: UIViewController, fromView: UIView)
    func speakerViewControllerDidRequestAddPreset(_ speakerViewController: SpeakerViewController, fromViewController:UIViewController, fromView: UIView)
    func speakerViewControllerDidRequestBrowseRadio(_ speakerViewController: SpeakerViewController, fromViewController:UIViewController, fromView: UIView)
    
    func speakerViewControllerDidRequestReconnect(_ speakerViewController: SpeakerViewController)
    func speakerViewControllerDidRequestCancelReconnect(_ speakerViewController: SpeakerViewController)
    
    func speakerViewControllerDidRequestShowSpotifyPlaylistWithId(_ spotifyPlaylistId:String, speakerViewController: SpeakerViewController)
    
}

class SpeakerViewController: UIViewController {
    
    var viewModel: SpeakerViewModel?
    weak var delegate : SpeakerViewControllerDelegate?
    
    var currentMainViewController: UIViewController?
    var presentInteractor: MiniToLargeViewInteractive!
    var dismissInteractor: MiniToLargeViewInteractive!
    var disableInteractivePlayerTransitioning = false
    var playerViewController: PlayerViewController!
    
    @IBOutlet weak var contentContainer: UIView!
    @IBOutlet weak var drawerContainer: UIView!
    @IBOutlet weak var drawerBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var nowplayingPreset: UIButton!
    @IBOutlet weak var nowPlayingTitle: MarqueeLabel!
    @IBOutlet weak var nowPlayingArtist: MarqueeLabel!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var miniSpotifyLogo: UIImageView!
    
    
    @IBOutlet weak var miniSpotifyLogoConstraint: NSLayoutConstraint!
    @IBOutlet weak var presetNumberVerticalConstraint: NSLayoutConstraint!
    
    @IBAction func onVolume(_ sender: UIButton) {
        
        self.delegate?.viewControllerDidRequestVolume(self, fromView: sender)
    }
    @IBAction func onBack(_ sender: AnyObject) {
        
        self.delegate?.speakerViewControllerDidRequestBack(self)
    }
    
    override func viewDidLoad() {
        
        playerViewController = UIStoryboard.player.nowPlayingViewController
        playerViewController.viewModel = viewModel?.playerViewModel
        playerViewController.delegate = self;
        playerViewController.transitioningDelegate = self;
        playerViewController.modalPresentationCapturesStatusBarAppearance = false
        playerViewController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        
        nowPlayingTitle.type = .continuous
        nowPlayingTitle.speed = .rate(70.0)
        nowPlayingTitle.animationCurve = .linear
        nowPlayingTitle.fadeLength = 5.0
        nowPlayingTitle.animationDelay = 4.0
        nowPlayingTitle.trailingBuffer = 30.0
        
        nowPlayingArtist.type = .continuous
        nowPlayingArtist.speed = .rate(70.0)
        nowPlayingArtist.animationCurve = .linear
        nowPlayingArtist.fadeLength = 5.0
        nowPlayingArtist.animationDelay = 4.0
        nowPlayingArtist.trailingBuffer = 30.0
        
        presentInteractor = MiniToLargeViewInteractive()
        presentInteractor.attachToViewController(self, withView: drawerContainer, presentViewController: playerViewController)
        
        dismissInteractor = MiniToLargeViewInteractive()
        dismissInteractor.attachToViewController(playerViewController, withView: playerViewController.view, presentViewController: nil)
        
        if let vm = viewModel {
            
            
            nowPlayingTitle.font = Fonts.UrbanEars.Medium(12)
            nowPlayingArtist.font = Fonts.UrbanEars.Regular(12)
            nowplayingPreset.titleLabel?.font = Fonts.UrbanEars.Light(25)
            
            vm.connectionStatus.asObservable().distinctUntilChanged().subscribe(weak: self, onNext: SpeakerViewController.updateForConnected).disposed(by: rx_disposeBag)
            
            vm.playerViewModel.miniPlayerLine1Text.asObservable().map{ $0?.uppercased() }.bind(to:nowPlayingTitle.rx.text).disposed(by: rx_disposeBag)
            vm.playerViewModel.miniPlayerLine2Text.asObservable().bind(to:nowPlayingArtist.rx.text).disposed(by: rx_disposeBag)
            
            let isPlaying = vm.playerViewModel.isPlayingOrBuffering.asObservable()
            let canStop = vm.playerViewModel.canStop.asObservable()
                
            Observable.combineLatest(isPlaying, canStop) { isPlaying, canStop -> UIImage in
                var buttonImage: UIImage! = nil
                if isPlaying {
                    buttonImage = canStop ? UIImage(named: "stop_icon") : UIImage(named: "pause_icon")
                } else {
                    buttonImage = UIImage(named: "play_icon")
                }
                return buttonImage
                }
                .subscribe(onNext:{ [weak self] buttonImage in
                    self?.playPauseButton.setImage(buttonImage, for: .normal)
                }).disposed(by: rx_disposeBag)
            
            
            
            let optionalStringComparer = {(rhs: String?, lhs: String?) in rhs == lhs}
            let playableItem = vm.clientSpeakerState.asObservable().map{ $0?.currentPlayableItem }
            let castAppName = vm.masterSpeakerState.asObservable().map{ $0?.castAppName }.distinctUntilChanged(optionalStringComparer)
            typealias PlayableItemInfo = (playableItem: PlayableItem?, castAppName: String?)
            
            Observable.combineLatest(playableItem, castAppName) {  (item: PlayableItemInfo) in
                return item
            }.subscribe(weak: self, onNext: SpeakerViewController.updateForCurrentlyPlayingItem).disposed(by: rx_disposeBag)
            
            
            vm.playerViewModel.playCaps.asObservable().map{ $0.canPause || $0.canStop }.map{ !$0 }.bind(to:playPauseButton.rx.isHidden).disposed(by: rx_disposeBag)
            
            
            let standby = vm.playerViewModel.isInStandby.asObservable()
            let connected = vm.connectionStatus.asObservable().map{ status -> Bool in
                switch status {
                case .disconnected,.connecting: return false
                case .connected, .idle, .paused: return true
                }
            }
            let notAudioSync = vm.masterSpeakerState.asObservable().map{ state -> Bool in
                return state?.currentMode?.isAudsync == false
            }.distinctUntilChanged()
            
           
            
            let showMiniPlayer = Observable.combineLatest(standby.skip(1), connected, notAudioSync) { standby, connected, notAudioSync in
                return !standby && connected && notAudioSync
            }
            showMiniPlayer.distinctUntilChanged().subscribe(onNext:{ [weak self] showMiniPlayer in
                self?.updateMiniPlayerVisible(showMiniPlayer, animated: true)
            }).disposed(by: rx_disposeBag)
            
            
            let showMiniPlayerFirstTime = Observable.combineLatest(standby, connected, notAudioSync) { standby, connected, notAudioSync in
                return !standby && connected && notAudioSync
                }.take(1)
            
            showMiniPlayerFirstTime.subscribe(onNext:{ [weak self] showMiniPlayer in
                self?.updateMiniPlayerVisible(showMiniPlayer, animated: false)
            }).disposed(by: rx_disposeBag)
            
            
            
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
    }
    
    func updateForConnected(_ connected: SpeakerConnectionStatus) {
        
        if let vm = viewModel {
            switch connected {
            case .connected, .idle, .paused:
                
                //this avoids recreating the view when cycling through cases that fall in the same bucket
                if !(self.currentMainViewController is PresetsViewController) {
                    let presetsViewController = UIStoryboard.player.presetsViewController
                    presetsViewController.delegate = self
                    presetsViewController.viewModel = vm.presetsViewModel
                    setMainContentViewController(presetsViewController)
                }
                
            case .disconnected(_):
                
                let speakerDisconnectedViewController = UIStoryboard.player.speakerDisconnectedViewController
                speakerDisconnectedViewController.viewModel = self.viewModel
                speakerDisconnectedViewController.delegate = self
                speakerDisconnectedViewController.view.backgroundColor = UIColor.clear
                setMainContentViewController(speakerDisconnectedViewController)
                
            case .connecting:
                
                let speakerConnectingViewController = UIStoryboard.player.speakerConnectingViewController
                speakerConnectingViewController.delegate = self
                
                setMainContentViewController(speakerConnectingViewController)
            }
            
        }
    }
    
    func setMainContentViewController(_ viewController: UIViewController) {
        
        if currentMainViewController == nil {
            
            self.addChildViewController(viewController)
            contentContainer.addSubview(viewController.view)
            viewController.view.translatesAutoresizingMaskIntoConstraints = false
            constrain(viewController.view) { view in
                view.edges == view.superview!.edges
            }
            viewController.didMove(toParentViewController: self)
            currentMainViewController = viewController
        } else {
            
            self.addChildViewController(viewController)
            if let viewControllerToRemove = currentMainViewController {
                
                viewControllerToRemove.willMove(toParentViewController: nil)
                
                
                contentContainer.addSubview(viewController.view)
                viewController.view.translatesAutoresizingMaskIntoConstraints = false;
                constrain(viewController.view) { view in
                    view.edges == view.superview!.edges
                }
                
                self.addChildViewController(viewController)
                viewController.didMove(toParentViewController: self)
                
                viewControllerToRemove.view.removeFromSuperview()
                viewControllerToRemove.removeFromParentViewController()
            }
            currentMainViewController = viewController
            
        }
    }
    
    func updateMiniPlayerVisible(_ visible: Bool, animated: Bool) {
        
        
        var bottomSpacing: CGFloat = 0
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate ,
            let length = appDelegate.window?.rootViewController?.topLayoutGuide.length {
            bottomSpacing = length
        }
        self.drawerContainer.isUserInteractionEnabled = visible
        
        
        if visible && drawerBottomConstraint.constant != 50{
            
            drawerBottomConstraint.constant = 50
        }
        if !visible && drawerBottomConstraint.constant != -bottomSpacing {
            
            drawerBottomConstraint.constant = -bottomSpacing
            
            if self.presentedViewController == playerViewController {
                
                self.dismiss(animated: animated, completion: nil)
            }
        }
        
        
        if animated {
            
            UIView.animate(withDuration: 0.25, delay: 0.0, options: [.beginFromCurrentState], animations: { [weak self] in
                self?.drawerContainer.layoutIfNeeded()
            })
            
        } else {
            
            self.view.layoutIfNeeded()
        }
    }
    
//    func updateMiniPlayerVisible(_ visible: Bool, animated: Bool) {
//
//        self.drawerContainer.isUserInteractionEnabled = visible
//
//        if visible && drawerBottomConstraint.constant != 0 {
//
//            drawerBottomConstraint.constant = 0
//            drawerContainer.isHidden = false
//
//        }
//        if !visible && drawerBottomConstraint.constant != -50.0 {
//
//            drawerBottomConstraint.constant = -50
//            drawerContainer.isHidden = true
//
//            if self.presentedViewController == playerViewController {
//
//                self.dismiss(animated: animated, completion: nil)
//            }
//        }
//
//        if animated {
//
//            UIView.animate(withDuration: 0.25, delay: 0.0, options: [.beginFromCurrentState], animations: { [weak self] in
//                self?.drawerContainer.layoutIfNeeded()
//            }, completion: nil)
//
//        } else {
//
//            //self.view.layoutIfNeeded()
//        }
//    }
//
    func updateForCurrentlyPlayingItem(_ playableItem: PlayableItem?, castAppName: String?) {
        
        nowplayingPreset.setTitle(nil, for: .normal)
        nowplayingPreset.setImage(nil, for: .normal)
        
        var hideSpotifyLogo = true
        var cloudIcon = false
        
        if let item = playableItem {
           
            switch item.selectableType {
            case .preset(let number):
                nowplayingPreset.setTitle(String(number), for: .normal)
                cloudIcon = false
                
                if let preset = item as? Preset {
                    
                    var spotifyPreset = false
                    
                    if let type = preset.type {
                        switch type {
                        case PresetType.spotify: spotifyPreset = true
                        case PresetType.internetRadio, PresetType.unknown: spotifyPreset = false
                        }
                    }
                    
                    hideSpotifyLogo = !spotifyPreset
                }
                
            case .cloud:
                nowplayingPreset.setImage(UIImage(named:item.selectableType.smallIconName!), for: .normal)
                cloudIcon = true
                
                if let currentMode = viewModel?.clientSpeakerState.value?.currentMode {
                    
                    var spotifyCloud = false
                    if (currentMode.isCast && castAppName == "Spotify") || currentMode.isSpotify {
                        spotifyCloud = true
                    }
                    
                    hideSpotifyLogo = !spotifyCloud
                }
                
                break;
                
            default:
                nowplayingPreset.setImage(UIImage(named:item.selectableType.smallIconName!), for: .normal)
                cloudIcon = true
                break;
            }
        }
        
        
        self.presetNumberVerticalConstraint.constant = cloudIcon ? 0.0 : -3.0
        self.miniSpotifyLogo.superview?.layoutIfNeeded()
        if hideSpotifyLogo {
            self.miniSpotifyLogoConstraint.constant = -25
        } else {
            self.miniSpotifyLogoConstraint.constant = cloudIcon ? 0.0 : -10.0
        }
        
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: [.beginFromCurrentState], animations: { [weak self] in
            
            guard let `self` = self else { return }
            self.miniSpotifyLogo.superview?.layoutIfNeeded()
            self.miniSpotifyLogo.alpha = hideSpotifyLogo ? 0.0 : 1.0
            
        }, completion: nil)
    }
    
    @IBAction func onShowPlayer(_ sender: AnyObject) {
        
        self.disableInteractivePlayerTransitioning = true
        
        self.definesPresentationContext = true
        self.present(playerViewController, animated: true, completion: { [weak self] in
            
            self?.disableInteractivePlayerTransitioning = false;
        })
    }
    
    @IBAction func onPlayPause(_ sender: AnyObject) {
        
        self.viewModel?.playPause()
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)?) {
        
        self.disableInteractivePlayerTransitioning = true
        super.dismiss(animated: flag, completion: { [weak self] in
            self?.disableInteractivePlayerTransitioning = false
            if let c = completion {
                c()
            }
            })
    }
}

extension SpeakerViewController: UIGestureRecognizerDelegate {
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        return false
    }
}

extension SpeakerViewController: PresetsViewControllerDelegate {
    
    func presetsViewControllerDidRequestAirplay(_ viewController: PresetsViewController) {
        
        self.delegate?.speakerViewControllerDidRequestAirplay(self)
    }
    
    func presetsViewControllerDidRequestSpotifyConnect(_ viewController: PresetsViewController) {
        
        self.delegate?.speakerViewControllerDidRequestSpotifyConnect(self)
    }
    
    func presetsViewControllerDidRequestGoogleCast(_ viewController: PresetsViewController) {
        
        self.delegate?.speakerViewControllerDidRequestGoogleCast(self)
    }
    
    func presetsViewControllerDidRequestPlayRadio(_ viewController: PresetsViewController, view: UIView) {
        
        self.delegate?.speakerViewControllerDidRequestBrowseRadio(self, fromViewController: viewController, fromView: view)
    }
    
    func presetsViewControllerDidRequestPresetsDisabledInfo(_ viewController: PresetsViewController) {
     
        self.delegate?.speakerViewControllerDidRequestPresetDisabledInfo(self)
    }
    
    func presetsViewControllerDidRequestShowSpotifyPlaylist(_ spotifyPlaylistId: String) {
        
        self.delegate?.speakerViewControllerDidRequestShowSpotifyPlaylistWithId(spotifyPlaylistId, speakerViewController: self)
    }
}

extension SpeakerViewController: MiniToLargeAnimatorDataSource {
    
    func transitioningViewForAnimator(_ animator: MiniToLargeAnimator) -> UIView {
        
        return self.drawerContainer.snapshotView(afterScreenUpdates: false) ?? UIView()
    }
}

extension SpeakerViewController: PlayerViewControllerDelegate {
    
    func playerViewControllerDidRequestDismiss(_ playerViewController: PlayerViewController) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func playerViewControllerDidRequestVolume(_ playerViewController: PlayerViewController, fromView: UIView) {
        
        self.delegate?.viewControllerDidRequestVolume(playerViewController, fromView: fromView)
    }
    
    func playerViewControllerDidRequestBrowse(_ playerViewController: PlayerViewController, fromView: UIView) {
        
        self.delegate?.speakerViewControllerDidRequestBrowseRadio(self, fromViewController: playerViewController, fromView: fromView)
    }
    
    func playerViewControllerDidRequestOpenPlaylistInfo(_ playerViewController: PlayerViewController) {
        
        if let vm = playerViewController.viewModel {
        
            if let playlistURI = vm.masterState.value?.spotifyPlaylistURI {
                self.delegate?.speakerViewControllerDidRequestShowSpotifyPlaylistWithId(playlistURI, speakerViewController: self)
            }
        }
    }
    
    func playerViewControllerDidRequestAddPreset(_ playerViewController: PlayerViewController, fromView: UIView) {
        
        self.delegate?.speakerViewControllerDidRequestAddPreset(self, fromViewController: playerViewController, fromView: fromView)
    }
}

extension SpeakerViewController: SpeakerDisconnectedViewControllerDelegate {
    
    func speakerDisconnectedDidRequestReconnect(_ viewController: SpeakerDisconnectedViewController) {
        
        self.delegate?.speakerViewControllerDidRequestReconnect(self)
    }
    
    func speakerDisconnectedDidRequestDisconnect(_ viewController: SpeakerDisconnectedViewController) {
        
        self.delegate?.speakerViewControllerDidRequestBack(self)
    }
}

extension SpeakerViewController: SpeakerConnectingViewControllerDelegate {
    
    func speakerConnectingDidRequestCancel(_ viewController: SpeakerConnectingViewController) {
        
        self.delegate?.speakerViewControllerDidRequestCancelReconnect(self)
    }
}

extension SpeakerViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        let animator = MiniToLargeAnimator()
        animator.initialY = self.drawerContainer.bounds.size.height
        animator.transitionType = .dismiss
        animator.dataSource = self
        return animator
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        let animator = MiniToLargeAnimator()
        animator.initialY = self.drawerContainer.bounds.size.height
        animator.transitionType = .present
        animator.dataSource = self
        
        return animator
    }
    
    func interactionControllerForPresentation(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        if self.disableInteractivePlayerTransitioning {
            return nil
        }
        return self.presentInteractor
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        if self.disableInteractivePlayerTransitioning {
            return nil
        }
        return self.dismissInteractor
    }
}

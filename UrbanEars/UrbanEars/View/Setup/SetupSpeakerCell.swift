//
//  MenuTableViewCell.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 09/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit

class SetupSpeakerCell: UITableViewCell {

    @IBOutlet weak var menuItemImageView: UIImageView!
    @IBOutlet weak var menuItemLabel: UILabel!
    @IBOutlet weak var menuItemIndicatorImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
        let customColorView = UIView()
        customColorView.backgroundColor = UIColor(white: 1.0, alpha: 0.3);
        self.selectedBackgroundView =  customColorView;
    }
}

//
//  SetupListViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 15/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxSwift
import MinuetSDK

protocol SetupListViewControllerDelegate:class {
    
    func setupListViewControllerDidPickUnconfiguredSpeaker(_ speaker:UnconfiguredSpeaker, setupListViewController: SetupListViewController)
    func setupListViewControllerRequestCancel(_ setupListViewController:SetupListViewController)
}

enum SetupListState {
    
    case loading
    case list
}

class SetupListViewController: UIViewController,UITableViewDelegate {

    
    @IBOutlet weak var setupListContainerView: UIView!
    
    @IBOutlet weak var speakerCountLabel: UILabel!
    @IBOutlet weak var separatorViewHeight: NSLayoutConstraint!

    @IBOutlet weak var tableView: UITableView!
    var viewModel : SetupNetworkViewModel!
    weak var delegate: SetupListViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        separatorViewHeight.constant = 0.5
        
        speakerCountLabel.font = Fonts.UrbanEars.Regular(17)
        
        tableView.rx.setDelegate(self).disposed(by: rx_disposeBag)
        
        setupListContainerView.isHidden = false
        
        
        if let vm = viewModel {
            
            //populate unconfigured speakers in the list
            vm.visibleUnconfiguredSpeakers.asObservable().bind(to:tableView.rx.items(cellIdentifier:"setupSpeakerItem", cellType: SetupSpeakerCell.self)) { (row, element, cell) in
                let attributedText = NSMutableAttributedString()
                
                let nameFont = Fonts.UrbanEars.Regular(18)
                let nameColor = UIColor.white
                let nameAttributes = [NSFontAttributeName:nameFont, NSForegroundColorAttributeName:nameColor]
                
                let colorFont = Fonts.UrbanEars.Regular(15)
                let colorColor = UIColor(red:0.67, green:0.68, blue:0.68, alpha:1)
                let colorAttributes = [NSFontAttributeName:colorFont, NSForegroundColorAttributeName:colorColor]
                
                let configureWithSpeakerImage: (_ speakerImage: SpeakerImage,_ nameIndex: Int) -> () = { speakerImage, nameIndex in
                    
                    if let model = speakerImage.displayModel, let color = speakerImage.displayColor {
                        //let macfragment = element.model.replacingOccurrences(of: "\(speakerImage.name!) ", with: "")
                        //attributedText.append(NSAttributedString(string: "\(model) \(macfragment)\n", attributes: nameAttributes))
                        var modelText = model
                        var colorToUse : String
                        if color == "Gold Fish" {
                            colorToUse = "Goldfish"
                        } else {
                            colorToUse = color
                        }
                        if nameIndex > 0 {
                            modelText = "\(model) (\(nameIndex + 1))"
                        }
                        attributedText.append(NSAttributedString(string: "\(modelText)\n", attributes: nameAttributes))
                        attributedText.append(NSAttributedString(string: colorToUse, attributes: colorAttributes))
                    } else {
                        
                        attributedText.append(NSAttributedString(string: element.ssid + " ", attributes: nameAttributes))
                    }
                    
                    if let speakerImageName = speakerImage.listItemImageName {
                        cell.menuItemImageView?.image  = UIImage(named: speakerImageName)
                    } else {
                        cell.menuItemImageView?.image  = UIImage(color: UIColor(white: 0.0, alpha: 0.2))
                    }
                }
                
                if let speakerImage =  ExternalConfig.sharedInstance.speakerImages?.speakerImages?.filter({ speakerImage in element.ssid.hasPrefix(speakerImage.ssid!) }).first {
                    
                    configureWithSpeakerImage(speakerImage, element.nameIndex)
                }
                else if let speakerImage =  ExternalConfig.sharedInstance.speakerImages?.speakerImages?.filter({ speakerImage in element.model.hasPrefix(speakerImage.name!) }).first {
                    
                    configureWithSpeakerImage(speakerImage, element.nameIndex)
                }
                else {
                    attributedText.append(NSAttributedString(string: element.ssid + " ", attributes: nameAttributes))
                    cell.menuItemImageView?.image  = UIImage(named: "list_item_placeholder")
                }
                
                cell.menuItemLabel?.attributedText = attributedText
                
                }.disposed(by: rx_disposeBag)
            
            //bind number of speakers text to "Found x speakers label"
            vm.visibleUnconfiguredSpeakers.asObservable().map { [weak self] e in
                
                return self?.displayTextForSpeakerCount(e.count) ?? ""
            }.bind(to:speakerCountLabel.rx.text).disposed(by: rx_disposeBag)

        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    func displayTextForSpeakerCount(_ count:Int)->String {
        
        return Localizations.Setup.List.FoundSpeakersWithCount(count)
    }
    
    
    @IBAction func onClose(_ sender: AnyObject) {
        
        self.delegate?.setupListViewControllerRequestCancel(self)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let speaker = viewModel.visibleUnconfiguredSpeakers.value[indexPath.row]
        delegate?.setupListViewControllerDidPickUnconfiguredSpeaker(speaker, setupListViewController: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt editingStyleForRowAtIndexPath: IndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.none
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

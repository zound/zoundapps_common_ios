//
//  SetupPresetCell.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit

class SetupPresetCell: UITableViewCell {

    @IBOutlet weak var menuItemImageView: UIImageView!
    @IBOutlet weak var menuItemLabel: UILabel!
    @IBOutlet weak var menuItemNumberLabel: UILabel!

    @IBOutlet weak var menuItemDescriptionLabe: UILabel!
    override func awakeFromNib() {
        
        super.awakeFromNib()
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
        menuItemLabel.font = Fonts.ListItemFont
        menuItemNumberLabel.font = Fonts.UrbanEars.Regular(48)
        menuItemDescriptionLabe.font = Fonts.UrbanEars.Regular(14)
        
        let customColorView = UIView()
        customColorView.backgroundColor = UIColor(white: 1.0, alpha: 0.3);
        self.selectedBackgroundView =  customColorView;

    }
}

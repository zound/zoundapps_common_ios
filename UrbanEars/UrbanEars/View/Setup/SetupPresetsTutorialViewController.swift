//
//  SetupPresetsIntroViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit

protocol SetupPresetsTutorialViewControllerDelegate: class {
    
    func setupPresetsTutorialDidFinish(_ viewController:SetupPresetsTutorialViewController)
    func setupPresetsTutorialDidRequestBack(_ viewController:SetupPresetsTutorialViewController)
}

class SetupPresetsTutorialViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentTopLabel: UILabel!
    @IBOutlet weak var contentBottomLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    
    var viewModel: SetupPresetsViewModel?
    weak var delegate: SetupPresetsTutorialViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = Localizations.Setup.PresetsTutorial.Title
        titleLabel.font = Fonts.UrbanEars.Medium(23)
        
        contentTopLabel.text = Localizations.Setup.PresetsTutorial.ContentTop
        contentTopLabel.font = Fonts.MainContentFont
        
        contentBottomLabel.text = Localizations.Setup.PresetsTutorial.ContentBottom
        contentBottomLabel.font = Fonts.MainContentFont
        
        UIView.setAnimationsEnabled(false)
        
        nextButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Setup.PresetsTutorial.Buttons._Continue), for: .normal)
        nextButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    @IBAction func onDone(_ sender: AnyObject) {
        
        self.delegate?.setupPresetsTutorialDidFinish(self)
    }
    
    @IBAction func onBack(_ sender: AnyObject) {
        
        self.delegate?.setupPresetsTutorialDidRequestBack(self)
    }
    
}

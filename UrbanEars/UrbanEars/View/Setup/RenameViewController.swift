//
//  HelloViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import Toast_Swift
import MinuetSDK
import Zound

protocol RenameViewControllerDelegate: class {
    
    func renameViewControllerDidRequestSkip(_ viewController: RenameViewController)
    func renameViewControllerDidRequestBack(_ viewController: RenameViewController)
    func renameViewControllerDidRequestTutorial(_ viewController: RenameViewController)

}

enum RenameViewState {
    
    case question
    case textField
    case renaming
    case done
}

class RenameViewController: UIViewController {
    
    var viewModel: SetupRenameViewModel?
    weak var delegate: RenameViewControllerDelegate?
    
    @IBOutlet weak var speakerImage: UIImageView!
    @IBOutlet weak var imageDescriptionLabel: UILabel!
    @IBOutlet weak var speakerNameTextField: UITextField!
    @IBOutlet weak var speakerNameTextFieldLine: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    @IBOutlet weak var learnContainer: UIView!
    @IBOutlet weak var renameContainer: UIView!
    
    @IBOutlet weak var topContainer: UIView!
    @IBOutlet weak var topContainerBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var renameButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var skipTutorialButton : UIButton!
    @IBOutlet weak var learnButton: UIButton!

    
    var tapGesture:UITapGestureRecognizer!
    
    override func viewDidLoad() {
        
        UIView.setAnimationsEnabled(false)
        renameButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Setup.Rename.Buttons.Rename), for: .normal)
        skipButton.setAttributedTitle(Fonts.ButtonFont.AttributedSecondaryButtonWithString(Localizations.Appwide.Skip), for: .normal)
        learnButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Setup.Rename.Buttons.LearnHowItWorks), for: .normal)
        skipTutorialButton.setAttributedTitle(Fonts.ButtonFont.AttributedSecondaryButtonWithString(Localizations.Appwide.Skip), for: .normal)
        
        learnButton.layoutIfNeeded()
        renameButton.layoutIfNeeded()
        skipButton.layoutIfNeeded()
        skipTutorialButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        
        imageDescriptionLabel.font = Fonts.MainContentFont
        speakerNameTextField.font = Fonts.MainContentFont
        
        Keyboard.keyboardFrameChangeAnimationInfoObservable().subscribe(onNext: {[weak self] (animationInfo:KeyboardAnimationTuple) in
            self?.animateContentUsingKeyboardFrameAnimationInfo(animationInfo)
        })
        .disposed(by: rx_disposeBag)
        
        if let viewModel = self.viewModel {
            
            viewModel.currentRenameStatus.asObservable().map{status in status ?? ""}.bind(to:self.imageDescriptionLabel.rx.text).disposed(by: rx_disposeBag)
            
            
            let color = viewModel.speaker.color
            if let speakerName = ExternalConfig.sharedInstance.speakerImageForColor(color)?.heroImageName {
                speakerImage.image = UIImage(named: speakerName)
            } else {
                speakerImage.image = UIImage(named: "hero_placeholder")
            }
            
            
            viewModel.friendlyName.asObservable()
                .bind(to:self.speakerNameTextField.rx.text).disposed(by: rx_disposeBag)
            self.speakerNameTextField.rx.text.skip(1).bind(to:viewModel.friendlyName).disposed(by: rx_disposeBag)
        }
        
        speakerNameTextField.delegate = self
        transitionToState(.question, animated: false, completed: nil)
    }
    
    
    func transitionToState(_ state: RenameViewState, animated: Bool, completed: OptionalClosure) {
        
        switch state {
            
        case .question:
            UIView.setAlphaOfView(speakerNameTextField, visible:false, animated: animated, completed: nil)
            UIView.setAlphaOfView(speakerNameTextFieldLine, visible:false, animated: animated, completed: nil)
            UIView.setAlphaOfView(imageDescriptionLabel, visible:true, animated: animated, completed: completed)
            UIView.setAlphaOfView(activityIndicator, visible:false, animated: animated, completed: completed)
            activityIndicator.stopAnimating()
            
            UIView.setAlphaOfView(learnContainer, visible: false, animated: animated, completed: nil)
            UIView.setAlphaOfView(renameContainer, visible: true, animated: animated, completed: nil)
            
        case .textField:
            UIView.setAlphaOfView(speakerNameTextField, visible:true, animated: animated, completed: completed)
            UIView.setAlphaOfView(speakerNameTextFieldLine, visible:true, animated: animated, completed: nil)
            UIView.setAlphaOfView(imageDescriptionLabel, visible:false, animated: animated, completed: nil)
            UIView.setAlphaOfView(activityIndicator, visible:false, animated: animated, completed: completed)
            
            activityIndicator.stopAnimating()
            
            UIView.setAlphaOfView(learnContainer, visible: false, animated: animated, completed: nil)
            UIView.setAlphaOfView(renameContainer, visible: false, animated: animated, completed: nil)
            
        case .renaming:
            UIView.setAlphaOfView(speakerNameTextField, visible:false, animated: animated, completed: nil)
            UIView.setAlphaOfView(speakerNameTextFieldLine, visible:false, animated: animated, completed: nil)
            UIView.setAlphaOfView(imageDescriptionLabel, visible:false, animated: animated, completed: nil)
            UIView.setAlphaOfView(activityIndicator, visible:true, animated: animated, completed: completed)
            activityIndicator.startAnimating()
            
            UIView.setAlphaOfView(learnContainer, visible: false, animated: animated, completed: nil)
            UIView.setAlphaOfView(renameContainer, visible: false, animated: animated, completed: nil)
            
            
        case .done:
            UIView.setAlphaOfView(speakerNameTextField, visible:false, animated: animated, completed: nil)
            UIView.setAlphaOfView(speakerNameTextFieldLine, visible:false, animated: animated, completed: nil)
            UIView.setAlphaOfView(imageDescriptionLabel, visible:true, animated: animated, completed: nil)
            UIView.setAlphaOfView(activityIndicator, visible:false, animated: animated, completed: completed)
            activityIndicator.stopAnimating()
            
            UIView.setAlphaOfView(learnContainer, visible: true, animated: animated, completed: nil)
            UIView.setAlphaOfView(renameContainer, visible: false, animated: animated, completed: nil)
        }
    }
    
    
    @IBAction func onSkipTutorial(_ sender: AnyObject) {
        
        self.delegate?.renameViewControllerDidRequestSkip(self)
    }
    
    @IBAction func onKeepName(_ sender: AnyObject) {
        
        viewModel?.changeFriendlyNameStatus.value = .changed
        transitionToState(.done, animated: true, completed: nil)
        
    }
    
    @IBAction func onTutorial(_ sender: AnyObject) {
        
        self.delegate?.renameViewControllerDidRequestTutorial(self)
    }

    
    @IBAction func onBack(_ sender: AnyObject) {
        self.delegate?.renameViewControllerDidRequestBack(self)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
}

extension RenameViewController: UITextFieldDelegate {
    
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let allCharactersValid = string.allCharactersValidAsSpeakerName
        
        let maxLength = maximumCharactersInSpeakerName
        let currentString: NSString = textField.text as NSString? ?? ""
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        let correctLength = (newString.length <= maxLength)
        
        return (allCharactersValid && correctLength);
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        onKeyboardDismissedWithDone()
        textField.resignFirstResponder()
        return false
    }
    
    func onKeyboardDismissedWithDone() {
        
        uninstallCancelRenameGestureRecognizer()
        let friendlyName = self.viewModel?.friendlyName.value?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if let friendlyName = friendlyName {
            if friendlyName.count > 0 {
                
                self.viewModel!.saveFriendlyName()
                transitionToState(.renaming, animated: true, completed: nil)
                self.viewModel!.changeFriendlyNameStatus.asObservable()
                    .filter({ $0 == ChangeFriendlyNameStatus.changed})
                    .take(1)
                    .subscribe(onNext:{[weak self] _ in
                        self?.transitionToState(.done, animated: true, completed: nil)
                    }).disposed(by: rx_disposeBag)
            } else {
                self.onRename(self)
            }
        }
    }
    
    
    @IBAction func onRename(_ sender: AnyObject) {
        
        transitionToState(.textField, animated: true, completed: { [weak self]  in
            
            if let textField = self?.speakerNameTextField {
                textField.becomeFirstResponder()
            }
        })
        NotificationCenter.default
            .rx.notification(NSNotification.Name.UIKeyboardDidShow)
            .take(1).map{_ in return}
            .subscribe(onNext:{ [weak self] next in self?.onKeyboardShownWhileRenaming()})
            .disposed(by: rx_disposeBag)
    }
    
    func installCancelRenameGestureRecognizer() {
        
        tapGesture = UITapGestureRecognizer()
        tapGesture.rx.event.map{_ in return}.take(1).subscribe(onNext:{ [weak self] _ in self?.onCancelRename() }).disposed(by: rx_disposeBag)
        self.view.addGestureRecognizer(tapGesture)
    }
    
    func uninstallCancelRenameGestureRecognizer() {
        
        if tapGesture != nil {
            self.view.removeGestureRecognizer(tapGesture)
            tapGesture = nil
        }
        
    }
    
    func onKeyboardShownWhileRenaming() {
        
        let textField = self.speakerNameTextField
        textField?.selectedTextRange = textField?.textRange(from: (textField?.beginningOfDocument)!, to: (textField?.endOfDocument)!)
        installCancelRenameGestureRecognizer()
    }
    
    func onCancelRename() {
        self.speakerNameTextField.resignFirstResponder()
        self.viewModel?.cancelSetFriendlyName()
        uninstallCancelRenameGestureRecognizer()
        transitionToState(.question, animated: true, completed: nil)
    }
    
    func animateContentUsingKeyboardFrameAnimationInfo(_ animationInfo: KeyboardAnimationTuple) {
        
        if let view = self.view {
            let keyboardFrame  = view.convert(animationInfo.frame, from: self.view.window)
            self.topContainerBottomConstraint.constant =  max((view.bounds.height)+20-learnContainer.frame.minY,(view.bounds.height)-(keyboardFrame.origin.y))
            UIView.animate(withDuration: animationInfo.animationDuration,
                           delay: 0.0,
                           options: UIViewAnimationOptions(rawValue: UInt(animationInfo.animationCurve << 16)),
                           animations: { [weak view] in
                            view?.layoutIfNeeded()
                },
                           completion: nil)
        }
    }
}

//
//  SettingsSettingCell.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 19/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

protocol SettingsSwitchCellDelegate: class {
    
    func settingsSwitchCellDelegateDidSwitchShareUsageData(on: Bool)
    
}

class SettingsSwitchCell: UITableViewCell {

    @IBOutlet weak var settingSwitch: UISwitch!
    @IBOutlet weak var switchLabel: UILabel!
    let settingVariable: Variable<CastSetting?> = Variable(nil)
    
    weak var delegate: SettingsSwitchCellDelegate?
    
    var setting: CastSetting? {
        
        get {
            return settingVariable.value
        }
        set {
            settingVariable.value = newValue
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
    }
    @IBOutlet weak var settingLabel: UILabel!
    
    private let castViewModelVariable: Variable<SettingsCastViewModel?> = Variable(nil)
    var castViewModel: SettingsCastViewModel? {
        
        get {
            return castViewModelVariable.value
        }
        set {
            castViewModelVariable.value = newValue
        }
    }
    
    override func awakeFromNib() {
        
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
        settingLabel.font = Fonts.ListItemFont
        
        let customColorView = UIView()
        customColorView.backgroundColor = UIColor(white: 1.0, alpha: 0.3);
        self.selectedBackgroundView =  customColorView;
        
        self.selectionStyle = .none
        
        let shareUsageData = castViewModelVariable.asObservable().map{ viewModel -> Observable<Bool> in
            guard let viewModel = viewModel else {return Observable.empty()}
            return viewModel.shareUsageData.asObservable()
        }.switchLatest()
        
        shareUsageData.subscribe(onNext: { [weak self] shareUsageState in
            self?.settingSwitch.setOn(shareUsageState, animated: true)
            self?.updateSwitchLabelForValue(shareUsageState)
            }).disposed(by: rx_disposeBag)
        
        
        
        settingVariable.asObservable().map{ $0 != nil ?  $0!.localizedDisplayName : "" }.bind(to:settingLabel.rx.text).disposed(by: rx_disposeBag)

    }
    
    @IBAction func onSwitch(_ sender: AnyObject) {
        if let vm = castViewModel {
            vm.updateShareUsageDataNode(toState: settingSwitch.isOn)
            updateSwitchLabelForValue(settingSwitch.isOn)
        }
    }

    
    func updateSwitchLabelForValue(_ on: Bool) {
        
        UIView.transition(
            with: switchLabel,
            duration: 0.15,
            options: [.transitionCrossDissolve,.beginFromCurrentState],
            animations: { [weak self] in
                
                guard let `self` = self else { return }
                if on {
                    
                    let onText = Localizations.Settings.GoogleCast.SwitchOn
                    self.switchLabel.attributedText = Fonts.UrbanEars.Regular(10).AttributedTextWithString(onText, color: UIColor(white: 1.0, alpha: 0.8), letterSpacing: 1.5)
                } else {

                    let offText = Localizations.Settings.GoogleCast.SwitchOff
                    self.switchLabel.attributedText = Fonts.UrbanEars.Regular(10).AttributedTextWithString(offText, color: UIColor(white: 1.0, alpha: 0.8), letterSpacing: 1.5)
                }
                
            },
            completion: nil)
    }
}

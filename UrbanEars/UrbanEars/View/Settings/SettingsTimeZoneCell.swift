//
//  SettingsTimeZoneCell
//  UrbanEars
//
//  Created by Raul Andrisan on 16/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import MinuetSDK

class SettingsTimeZoneCell: UITableViewCell {

    let timeZoneVariable: Variable<MinuetSDK.TimeZone?> = Variable(nil)
    var timeZone: MinuetSDK.TimeZone? {
        
        get {
            return timeZoneVariable.value
        }
        set {
            timeZoneVariable.value = newValue
        }
    }
    
    var tickVisible: Bool {
        
        get {
            return !selectionTick.isHidden
        }
        set {
            selectionTick.isHidden =  !newValue
        }
    }
    @IBOutlet weak var timeZoneLabel: UILabel!
    @IBOutlet weak var selectionTick: UIImageView!
    
    override func awakeFromNib() {
        
        
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
        timeZoneLabel.font = Fonts.ListItemFont
        
        let customColorView = UIView()
        customColorView.backgroundColor = UIColor(white: 1.0, alpha: 0.3);
        self.selectedBackgroundView =  customColorView;
        
        timeZoneVariable.asObservable().map{ $0 != nil ?  $0!.displayName : "" }.bind(to:timeZoneLabel.rx.text).disposed(by: rx_disposeBag)
    }
}

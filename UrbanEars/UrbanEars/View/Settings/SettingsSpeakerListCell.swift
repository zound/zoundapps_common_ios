//
//  SettingsSpeakerListCell.swift
//  UrbanEars
//
//  Created by Robert Sandru on 06/12/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxSwift
import MinuetSDK

class SettingsStreamingQualityCell: UITableViewCell {
    
    @IBOutlet weak var settingLabel: UILabel!
    @IBOutlet weak var streamingQualityImageView: UIImageView!
    @IBOutlet weak var disclosureIndicatorImageView: UIImageView!
    
    override func awakeFromNib() {
        self.settingLabel.text = Localizations.Settings.StreamingQuality.Celllabel
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        let customColorView = UIView()
        customColorView.backgroundColor = UIColor(white: 1.0, alpha: 0.3);
        self.selectedBackgroundView =  customColorView;
    }
    
}

class SettingsSpeakerCell: UITableViewCell {
    
    @IBOutlet weak var speakerImageView: UIImageView!
    @IBOutlet weak var speakerNameLabel: UILabel!
    @IBOutlet weak var disclosureIndicatorImageView: UIImageView!
    
    fileprivate let viewModelVariable: Variable<Speaker?> = Variable(nil)
    var viewModel: Speaker? {
        
        set {
            viewModelVariable.value = newValue
        }
        get {
            return viewModelVariable.value
        }
    }
    override func awakeFromNib() {
        
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
        speakerNameLabel.font = Fonts.ListItemFont
        
        let customColorView = UIView()
        customColorView.backgroundColor = UIColor(white: 1.0, alpha: 0.3);
        self.selectedBackgroundView =  customColorView;
        
        viewModelVariable.asObservable().map{ $0 != nil ?  $0!.friendlyNameWithIndex : "" }.bind(to:speakerNameLabel.rx.text).disposed(by: rx_disposeBag)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let audioSystem = appDelegate.appCoordinator.audioSystem
        let isUnlocked = viewModelVariable.asObservable().map { (speaker : Speaker?) -> Bool in
            if let currentSpeaker = speaker {
                return audioSystem.isSpeakerUnlocked(currentSpeaker)
            }
            return false
        }
        
        let spkImage = viewModelVariable.asObservable()
            .map{ ExternalConfig.sharedInstance.speakerImageForColor($0?.color)?.listItemImageName }
            .map{ ($0 != nil ? UIImage(named:$0!) : UIImage(named: "list_item_placeholder")) }
        
        Observable.combineLatest(spkImage, isUnlocked) { (speakerImageObservable, isUnlocked) -> (speakerImageObservable: UIImage, isUnlocked: Bool) in
            
            return (speakerImageObservable: speakerImageObservable!, isUnlocked: isUnlocked)
            }.subscribe(onNext:{ [weak self] (speakerImageObservable: UIImage, isUnlocked: Bool) in
                
                if isUnlocked == true {
                    self?.updateSpeakerImage(speakerImg: UIImage(named: "list_item_prototype")!)
                } else {
                    self?.updateSpeakerImage(speakerImg: speakerImageObservable)
                }
                
            }).disposed(by: rx_disposeBag)
        
        viewModelVariable.asObservable()
            .map{ ExternalConfig.sharedInstance.speakerImageForColor($0?.color)?.connectable }
            .map{ $0 != nil ? $0! : false }.map(!)
            .bind(to:disclosureIndicatorImageView.rx.isHidden).disposed(by: rx_disposeBag)

        
    }
    
    func updateSpeakerImage(speakerImg : UIImage) {
        self.speakerImageView.contentMode = .scaleAspectFit
        self.speakerImageView.image = speakerImg
    }
    
}

//
//  VolumeCell.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 08/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import MinuetSDK
import Zound

class VolumeCell: UICollectionViewCell {
    
    @IBOutlet weak var speakerNameLabel: UILabel!
    @IBOutlet weak var speakerRoleLabel: UILabel!
    @IBOutlet weak var volumeSlider: AnimatedSlider!
    @IBOutlet weak var speakerImageView: UIImageView!
    
    fileprivate let viewModelVariable: Variable<SpeakerVolumeViewModel?> = Variable(nil)
    var viewModel: SpeakerVolumeViewModel? {
        get { return viewModelVariable.value }
        set { viewModelVariable.value = newValue }
    }
    
    override func awakeFromNib() {
        
        speakerNameLabel.font = Fonts.UrbanEars.Regular(17)
        
        volumeSlider.setThumbImage(UIImage(named: "small_volume_slider_thumb"), for: .normal)
        volumeSlider.setMinimumTrackImage(UIImage(named:"slider_track_min"), for: .normal)
        volumeSlider.setMaximumTrackImage(UIImage(named:"slider_track_max"), for: .normal)
        volumeSlider.addTarget(self, action: #selector(onVolumeTouchDown), for: .touchDown)
        volumeSlider.addTarget(self, action: #selector(onVolumeTouchUp), for: .touchUpInside)
        volumeSlider.addTarget(self, action: #selector(onVolumeTouchUp), for: .touchUpOutside)
        volumeSlider.addTarget(self, action: #selector(onVolumeTouchUp), for: .touchCancel)
        
        
        viewModelVariable.asObservable().map{ $0 == nil ? "" : $0!.speaker.friendlyNameWithIndex }
            .bind(to:speakerNameLabel.rx.text)
            .disposed(by: rx_disposeBag)
        
        viewModelVariable.asObservable()
            .map{ ExternalConfig.sharedInstance.speakerImageForColor($0?.speaker.color)?.listItemSmallImageName }
            .map{ ($0 != nil ? UIImage(named:$0!) : UIImage(named:"list_item_small_placeholder")) }
            .bind(to:speakerImageView.rx.image).disposed(by: rx_disposeBag)
        
        viewModelVariable.asObservable().unwrapOptional()
            .flatMapLatest{ $0.volume.asObservable() }
            .map{ $0.volume }
            .unwrapOptional()
            .map{ (volume: Int) -> Float in
                let steps = 32
                return Float(volume) / Float(steps)
            }
            .subscribe(onNext:{[weak self] normalizedVolume -> () in
                
                if self?.volumeSlider.willSetValueFromUserInteraction == false {
                    self?.setSliderToValue(normalizedVolume)
                }
            })
            .disposed(by: rx_disposeBag)
        
        viewModelVariable.asObservable().unwrapOptional()
            .flatMapLatest{ $0.volume.asObservable() }
            .map{ $0.mute }.unwrapOptional()
            .subscribe(onNext:{[weak self] mute in
                self?.volumeSlider.alpha = mute ? 0.3 : 1.0
            }).disposed(by: rx_disposeBag)
        
        viewModelVariable.asObservable().unwrapOptional()
            .flatMapLatest{ $0.isMulti.asObservable() }
            .subscribe(onNext: { [weak self] isMulti in
                self?.speakerRoleLabel.text = isMulti ? Localizations.Volume.Role.Multi : Localizations.Volume.Role.Solo
            }).disposed(by: rx_disposeBag)
    }
    
    func setSliderToValue(_ value: Float) {
        
        self.volumeSlider.setValue(value, animated: false)
    }
    
    @IBAction func onVolumeTouchUp(_ sender: AnyObject) {
        
        self.volumeSlider.willSetValueFromUserInteraction = false
        viewModel?.stopSettingVolume()
    }
    
    @IBAction func onVolumeTouchDown(_ sender: AnyObject) {
        
        self.volumeSlider.willSetValueFromUserInteraction = true
        viewModel?.startSettingVolume()
    }
    
    @IBAction func onChangedVolume(_ sender: AnyObject) {
        
        viewModel?.setVolume(volumeSlider.value)
    }
    
}

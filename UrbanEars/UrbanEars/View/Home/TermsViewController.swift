//
//  TermsViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 26/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

protocol TermsViewControllerDelegate:class {
    
    func termsViewControllerDidRequestWebBrowserForURL(_ url: URL, termsViewController: TermsViewController)
    func termsViewControllerDidAccept(_ termsViewController: TermsViewController)
    func termsViewControllerDidRequestBack(_ termsViewController: TermsViewController)
}


class TermsViewController: UIViewController {
    
    weak var delegate: TermsViewControllerDelegate?
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var termsTitle: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var termsTitleLabel: UILabel!
    @IBOutlet weak var termsTextView: UITextView!
    @IBOutlet weak var termsTextViewHeight: NSLayoutConstraint!
    @IBOutlet weak var ueTermsTitle: UILabel!
    @IBOutlet weak var ueTermsView: UITextView!
    @IBOutlet weak var ueTermsViewHeight: NSLayoutConstraint!
    
    typealias ReplaceToken = (text: String, link: String)
    
    
    let tokenReplacements = [
        "[terms_link]":
            ReplaceToken(text:Localizations.Terms.Text.GoogleTerms.TermsLink.Text, link:Localizations.Terms.Text.GoogleTerms.TermsLink.Link),
        "[privacy_link]":
            ReplaceToken(text:Localizations.Terms.Text.GoogleTerms.PrivacyLink.Text, link:Localizations.Terms.Text.GoogleTerms.PrivacyLink.Link),
        "[ue_eula_link]":
            ReplaceToken(text:Localizations.Terms.Text.UeTerms.TermsLink.Text, link:Localizations.Terms.Text.UeTerms.TermsLink.Link)]
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        termsTitle.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.Terms.Title)
        
        UIView.setAnimationsEnabled(false)
        
        acceptButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Terms.Buttons.Accept), for: .normal)
        
        acceptButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        
        termsTitleLabel.font = Fonts.UrbanEars.Regular(18)
        termsTitleLabel.textColor = UIColor.white
        termsTitleLabel.text = Localizations.Terms.Text.GoogleTerms.Title
        
        ueTermsTitle.font = Fonts.UrbanEars.Regular(18)
        ueTermsTitle.textColor = UIColor.white
        ueTermsTitle.text = Localizations.Terms.Text.UeTerms.Title
        
        let termsFont = Fonts.UrbanEars.Regular(14)
        let termsColor = UIColor("#BBBBBB")
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        paragraphStyle.lineSpacing = 2.0
        
        let termsAttributes = [NSFontAttributeName:termsFont,
                               NSForegroundColorAttributeName:termsColor,
                                NSParagraphStyleAttributeName: paragraphStyle]
        let linkAttributes = [NSFontAttributeName:termsFont,
                              NSForegroundColorAttributeName:termsColor,
                              NSUnderlineStyleAttributeName: NSNumber(value: NSUnderlineStyle.styleSingle.rawValue as Int)]
        
        fillGoogleTerms(termsAttributes, linkAttributes: linkAttributes, termsColor: termsColor, termsFont: termsFont)
        fillUETerms(termsAttributes, linkAttributes: linkAttributes, termsColor: termsColor, termsFont: termsFont)
       
        
        termsTextView.rx.observe(CGSize.self, "contentSize").map{ $0 != nil ? $0!.height : 0}.bind(to:termsTextViewHeight.rx.constant).disposed(by: rx_disposeBag)
        termsTextView.disableTextSelection()
        
        ueTermsView.rx.observe(CGSize.self, "contentSize").map{ $0 != nil ? $0!.height : 0}.bind(to:ueTermsViewHeight.rx.constant).disposed(by: rx_disposeBag)
        ueTermsView.disableTextSelection()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    func fillUETerms(_ termsAttributes: [String : AnyObject]?,linkAttributes: [String : AnyObject]?, termsColor: UIColor, termsFont: UIFont) {
        
        let termsText = Localizations.Terms.Text.UeTerms.Content
        let termsAttributedText = NSMutableAttributedString(string: termsText, attributes:  termsAttributes)
        for token in tokenReplacements.keys {
            let tokenRange = (termsAttributedText.string as NSString).range(of: token)
            if tokenRange.length != 0 {
                let replaceToken = tokenReplacements[token]!
                let linkAttributes = [
                    NSFontAttributeName:termsFont,
                    NSForegroundColorAttributeName:termsColor,
                    NSLinkAttributeName: replaceToken.link] as [String : Any]
                let link = NSAttributedString(string: replaceToken.text, attributes: linkAttributes)
                termsAttributedText.replaceCharacters(in: tokenRange, with: link)
            }
        }
        
        ueTermsView.attributedText = termsAttributedText
        ueTermsView.linkTextAttributes = linkAttributes
    }
    
    func fillGoogleTerms(_ termsAttributes: [String : AnyObject]?,linkAttributes: [String : AnyObject]?, termsColor: UIColor, termsFont: UIFont) {
        
        let termsText = Localizations.Terms.Text.GoogleTerms.Content
        let termsAttributedText = NSMutableAttributedString(string: termsText, attributes:  termsAttributes)
        for token in tokenReplacements.keys {
            let tokenRange = (termsAttributedText.string as NSString).range(of: token)
            if tokenRange.length != 0 {
                let replaceToken = tokenReplacements[token]!
                let linkAttributes = [
                    NSFontAttributeName:termsFont,
                    NSForegroundColorAttributeName:termsColor,
                    NSLinkAttributeName: replaceToken.link] as [String : Any]
                let link = NSAttributedString(string: replaceToken.text, attributes: linkAttributes)
                termsAttributedText.replaceCharacters(in: tokenRange, with: link)
            }
        }
        
        termsTextView.attributedText = termsAttributedText
        termsTextView.linkTextAttributes = linkAttributes
    }
    
    @IBAction func onBack(_ sender: AnyObject) {
        
        self.delegate?.termsViewControllerDidRequestBack(self)
    }
    
    @IBAction func onAccept(_ sender: AnyObject) {
        
        self.delegate?.termsViewControllerDidAccept(self)
    }
   
}

extension TermsViewController: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        
        self.delegate?.termsViewControllerDidRequestWebBrowserForURL(URL, termsViewController: self)
        return false
    }
    
    
}

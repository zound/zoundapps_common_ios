//
//  Fonts.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 20/07/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    
    public func AttributedTextWithString(_ text: String, color: UIColor = UIColor.black, letterSpacing: CGFloat = 0, lineSpacing: CGFloat = 0, lineHeightMultiple: CGFloat = 0.0) -> NSAttributedString {
        
        if lineSpacing != 0 {
            
            let paragraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
            paragraphStyle.lineSpacing = lineSpacing
            if lineHeightMultiple != 0.0 {
                paragraphStyle.lineHeightMultiple = lineHeightMultiple
            }
            return NSAttributedString(string: text, attributes: [NSKernAttributeName:letterSpacing,
                NSFontAttributeName:self,
                NSParagraphStyleAttributeName: paragraphStyle,
                NSForegroundColorAttributeName: color])
        }
        
        return NSAttributedString(string: text, attributes: [NSKernAttributeName:letterSpacing, NSFontAttributeName:self, NSForegroundColorAttributeName: color])
    }
    
    public func AttributedPrimaryButtonWithString(_ text: String) -> NSAttributedString {
        
        return AttributedTextWithString(text, color: UIColor.black, letterSpacing: 1.5, lineSpacing: 0)
    }
    
    public func AttributedSecondaryButtonWithString(_ text: String) -> NSAttributedString {
        
        return AttributedTextWithString(text, color: UIColor(white: 1.0, alpha: 0.8), letterSpacing: 1.5, lineSpacing: 0)
    }
    
    
    public func AttributedPageTitleWithString(_ text: String) -> NSAttributedString {
     
        return AttributedTextWithString(text, color: UIColor.white, letterSpacing: 1.5, lineSpacing: 0)
    }
    
}


public struct Fonts {
    
}

public extension Fonts {
 
    public static var ButtonFont: UIFont = UrbanEars.Regular(13)
    public static var PageTitleFont: UIFont = UrbanEars.Regular(15)
    public static var MainContentFont: UIFont = UrbanEars.Regular(17)
    public static var ListItemFont: UIFont = UrbanEars.Regular(17)
    
    public struct UrbanEars {
        
        
        public static func ExtraLight(_ fontSize: CGFloat) -> UIFont {
            
            return UIFont(name: "AkzidGrtskNext-ExtraLight", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        }
        
        public static func Light(_ fontSize: CGFloat) -> UIFont {
            
            return UIFont(name: "AkzidGrtskNext-Light", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        }
        
        public static func Regular(_ fontSize: CGFloat) -> UIFont {
            
            return UIFont(name: "AkzidGrtskNext-Regular", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        }
        
        public static func Medium(_ fontSize: CGFloat) -> UIFont {
            
            return UIFont(name: "AkzidGrtskNext-Med", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        }
        
        public static func Bold(_ fontSize: CGFloat) -> UIFont {
            
            return UIFont(name: "AkzidGrtskNext-Bold", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        }
        
        public static func ExtraBold(_ fontSize: CGFloat) -> UIFont {
            
            return UIFont(name: "AkzidGrtskNext-ExtraBold", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        }
        
        public static func Black(_ fontSize: CGFloat) -> UIFont {
            
            return UIFont(name: "AkzidGrtskNext-Black", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        }
        
        
        /* Italic */
        
        
        public static func ExtraLightItalic(_ fontSize: CGFloat) -> UIFont {
            
            return UIFont(name: "AkzidGrtskNext-ExtraLightIt", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        }
        
        public static func LightItalic(_ fontSize: CGFloat) -> UIFont {
            
            return UIFont(name: "AkzidGrtskNext-LightItalic", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        }
        
        public static func Italic(_ fontSize: CGFloat) -> UIFont {
            
            return UIFont(name: "AkzidGrtskNext-Italic", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        }
        
        public static func MediumItalic(_ fontSize: CGFloat) -> UIFont {
            
            return UIFont(name: "AkzidGrtskNext-MedIt", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        }
        
        public static func BoldItalic(_ fontSize: CGFloat) -> UIFont {
            
            return UIFont(name: "AkzidGrtskNext-BoldIt", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        }
        
        public static func ExtraBoldItalic(_ fontSize: CGFloat) -> UIFont {
            
            return UIFont(name: "AkzidGrtskNext-ExtraBoldIt", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        }
        
        public static func BlackItalic(_ fontSize: CGFloat) -> UIFont {
            
            return UIFont(name: "AkzidGrtskNext-BlackIt", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        }   
    }
}


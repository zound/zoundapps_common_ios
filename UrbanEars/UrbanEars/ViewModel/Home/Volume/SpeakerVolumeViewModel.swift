//
//  VolumeSpeakerViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 15/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import MinuetSDK

protocol SpeakerVolumeViewModelDelegate: class {
    
    func volumeSpeakerViewModel(_ viewModel: SpeakerVolumeViewModel, didSetVolume volume: Int)
    func didStartSettingVolumeOnVolumeSpeakerViewModel(_ viewModel: SpeakerVolumeViewModel)
    func didStopSettingVolumeOnVolumeSpeakerViewModel(_ viewModel: SpeakerVolumeViewModel)
    func didFinishSettingVolumeOnVolumeSpeakerViewModel(_ viewModel: SpeakerVolumeViewModel)
}

class SpeakerVolumeViewModel {
    
    weak var delegate: SpeakerVolumeViewModelDelegate?
    var speaker: Speaker
    let volume: Variable<SpeakerVolume>
    let isMulti: Variable<Bool>
    let speakerProvider: SpeakerProvider
    let disposeBag = DisposeBag()
    var updatingVolume = false
    var isSettingVolume = false
    var volumeOnStartSetting: Int?
    
    init(speaker:Speaker, speakerVolume: SpeakerVolume, speakerProvider: SpeakerProvider, isMulti: Bool) {
    
        self.speaker = speaker
        self.volume = Variable(speakerVolume)
        self.speakerProvider = speakerProvider
        self.isMulti = Variable(isMulti)
        
    }
    
    func startSettingVolume() {
        
        isSettingVolume = true
        self.delegate?.didStartSettingVolumeOnVolumeSpeakerViewModel(self)
    }
    
    func stopSettingVolume() {
        
        self.delegate?.didStopSettingVolumeOnVolumeSpeakerViewModel(self)
        if !updatingVolume {
            self.delegate?.didFinishSettingVolumeOnVolumeSpeakerViewModel(self)
        }
        isSettingVolume = false
    }
    
    func setVolume(_ normlizedVolume: Float) {
        
        let volume = normlizedVolume * 32.0
        let roundedVolume = Int(round(volume))
        
        if let currentVolume = self.volume.value.volume {
            if roundedVolume != currentVolume {
                
                self.volume.value.volume = roundedVolume
                self.volume.value.mute = false
                updateVolume(roundedVolume)
                
                self.delegate?.volumeSpeakerViewModel(self, didSetVolume: roundedVolume)
            }
        }
    }
    
    func updateVolume(_ volume: Int) {
        
        if updatingVolume == false {
            
            updatingVolume = true
            speakerProvider.setNode(ScalarNode.Volume, value: String(volume), forSpeaker: self.speaker).subscribe(onNext: { [weak self] _ in
                guard let `self` = self else { return }
                self.updatingVolume = false
                if let currentVolume = self.volume.value.volume  {
                    if currentVolume != volume || self.isSettingVolume {
                        self.updateVolume(currentVolume)
                    } else {
                        if self.isSettingVolume == false {
                            self.delegate?.didFinishSettingVolumeOnVolumeSpeakerViewModel(self)
                        }
                    }
                }
                
                }).disposed(by: disposeBag)
        }
    }
}

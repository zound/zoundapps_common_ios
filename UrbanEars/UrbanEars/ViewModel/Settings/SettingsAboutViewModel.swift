
//
//  SettingsSpeakerAbout.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 17/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import MinuetSDK

class SettingsAboutViewModel {
    
    let speaker: Speaker
    let provider: SpeakerProvider
    let aboutState: AboutState
    let updateAvailable : Bool
    
    let wifiSignal: Variable<Int>
    
    let ip_key = "ip_key"
    let name_key = "name_key"
    let modelName_key = "modelName_key"
    let modelColor_key = "modelColor_key"
    let mac_key = "mac_key"
    let model_key = "model_key"
    let fwVersion_key = "fwVersion_key"
    let buildVersion_key = "buildVersion_key"
    let wifiName_key = "wifiName_key"
    let wifiSignal_key = "wifiSignal_key"
    let castVersion_key = "castVersion_key"
    
    let infoDictVariable : Variable<[String : String]>
    
    let disposeBag = DisposeBag()
    
    init(aboutState: AboutState, speaker: Speaker, provider: SpeakerProvider, updateAvailable:Bool) {
    
        self.aboutState = aboutState
        self.speaker = speaker
        self.provider = provider
        self.updateAvailable = updateAvailable
        
        let modelName = aboutState.modelName ?? ""
        let modelColor = aboutState.colorName ?? ""
        
        var modelColorToUse : String
        if modelColor == "Gold Fish" {
            modelColorToUse = "Goldfish"
        } else {
            modelColorToUse = modelColor
        }
       
        let InfoDictionary = [
            name_key : speaker.friendlyName,
            model_key : "\(modelName) \(modelColorToUse)",
            wifiName_key : aboutState.wifiName ?? "",
            ip_key : speaker.ipAddress,
            mac_key : speaker.mac,
            fwVersion_key : aboutState.softwareVersion ?? "",
            castVersion_key : aboutState.castVersion ?? ""
        ]
        
        self.infoDictVariable = Variable(InfoDictionary)
        self.wifiSignal = Variable(aboutState.wifiSignal ?? 0)

        provider.pollNode(ScalarNode.WlanSignalStrength, atInterval: 1.5, onSpeaker: speaker).map{ $0 != nil ? (Int($0!) ?? 0) : 0 }.bind(to:self.wifiSignal).disposed(by: disposeBag)
    }
}

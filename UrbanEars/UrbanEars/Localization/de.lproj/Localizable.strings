/* Localizable.strings
  UrbanEars

  Created by Raul Andrisan on 11/07/16.
  Copyright © 2016 Zound Industries. All rights reserved. */

"appwide.note" = "HINWEIS";

"appwide.continue" = "WEITER";

"appwide.back" = "ZURÜCK";

"appwide.next" = "WEITER";

"appwide.cancel" = "ABBRECHEN";

"appwide.skip" = "ÜBERSPRINGEN";

"appwide.settings" = "EINSTELLUNGEN";

"appwide.go_to_website" = "ZUR WEBSITE";

"about.eula.content" = "Die vollständige Version des Endbenutzer-Lizenzvertrags kann auf der Urbanears-Website eingesehen werden.";

"about.eula.title" = "Endbenutzer-Lizenzvertrag";

"about.foss.content" = "Die vollständige Version der kostenlosen und Open-Source-Software kann auf der Urbanears-Website eingesehen werden.";

"about.foss.title" = "FOSS";

"about.privacy_policy.title" = "Datenschutzrichtlinie";

"about.menu_item.eula" = "Endbenutzer-Lizenzvertrag";

"about.menu_item.foss" = "Kostenlose und Open-Source-Software";

"about.title" = "ÜBER";

"about.version" = "Urbanears Connected\nVersion %@ (%@)";

"airplay_source.content" = "Mit AirPlay kannst du über das Kontrollzentrum eines beliebigen iOS-Geräts eine Musikquelle direkt auf deinen Lautsprecher streamen.";

"browse_radio.search_placeholder" = "Radiosender suchen";

"browse_radio.title" = "RADIO DURCHSUCHEN";

"cast_info.buttons.get_google_cast" = "GOOGLE HOME-APP ERHALTEN";

"cast_info.buttons.learn_more" = "MEHR ERFAHREN";

"cast_info.content" = "Dieser Lautsprecher spielt über das integrierte Chromecast.\n\nVerwende die App Google Home, um Gruppen für Cast-Multiroom einzurichten\n\nDeine Multiroom-Gruppen werden unter der Cast-Schaltfläche in deiner Musik-App angezeigt.";

"cast_switch.content" = "Durch Wechseln des Lautsprechers in den Multi-Mode wird diese integrierte Chromecast-Sitzung beendet. \n\nVerwende die App Google Home, um Gruppen für Cast-Multiroom einzurichten";

"CFBundleDisplayName" = "Verbunden";

"CFBundleName" = "Verbunden";

"google_cast_source.content" = "Dein Lautsprecher ist über das Cast-Symbol auf mehreren Apps verfügbar.";

"google_cast_source.get_cast_apps" = "CAST-AKTIVIERTE APPS SUCHEN";

"google_cast_source.note" = "Hinweis!\nUm Musik auf mehreren Lautsprechern gleichzeitig über Google Cast abzuspielen, muss mit der Google Cast-App eine Multiroom-Gruppe eingerichtet werden.";

"group_full.buttons.dismiss" = "VERSTANDEN";

"group_full.content" = "Du versuchst, im Multi-Mode einen sechsten Lautsprecher hinzuzufügen.\n\nWir freuen uns, dass du so viele Lautsprecher gekauft hast, aber zurzeit unterstützt das Urbanears-Lautsprechersystem den Multi-Mode nur für fünf Lautsprecher in einem Wi-Fi-Netz.";

"unlocked_module.content" = "%@ is a prototype unit and will therefore stop working by the end of 2017. \n\nPlease get in touch with your point of contact at Zound Industries.";

"unlocked_module.dismiss" = "GOT IT";

"help.contact.cannot_send_email.content" = "Die E-Mail-App ist nicht mit einem E-Mail-Konto konfiguriert. Bitte konfiguriere ein Konto, um E-Mails von hier zu senden";

"help.contact.cannot_send_email.copy_button" = "E-Mail in die Zwischenablage kopieren";

"help.contact.cannot_send_email.ok_button" = "OK";

"help.contact.cannot_send_email.title" = "E-Mail kann nicht gesendet werden";

"help.contact.content" = "Besuche unsere Website auf\nwww.urbanears.com\n\noder sende deine Fragen an\nsupport@urbanears.com";

"help.contact.title" = "KONTAKT";

"help.menu_item.contact" = "Kontakt";

"help.menu_item.online_manual" = "Online-Handbuch";

"help.menu_item.quick_guide" = "Kurzanleitung";

"help.online_manual.title" = "ONLINE-HANDBUCH";

"help.quick_guide.menu_item.presets" = "Voreinstellungen";

"help.quick_guide.menu_item.solo_multi" = "Solo/Multi";

"help.quick_guide.menu_item.speaker_knobs" = "Lautsprecher-Knöpfe";

"help.quick_guide.title" = "KURZANLEITUNG";

"help.title" = "HILFE";

"home.speaker.playing_source" = "%@";

"home.speaker.switch_label.multi" = "MULTI";

"home.speaker.switch_label.solo" = "SOLO";

"loading.loading_speakers" = "Nach Connected Speakers suchen ...";

"menu.about" = "Über";

"menu.add_speaker" = "Setup";

"menu.help" = "Hilfe";

"menu.shop_urban_ears" = "Kaufen";

"menu.speaker_settings" = "Einstellungen";
"no_speakers.buttons.refresh" = "AKTUALISIEREN";

"no_speakers.buttons.set_up_speakers" = "LAUTSPRECHER EINRICHTEN";

"no_speakers.description" = "In diesem Wi-Fi-Netz scheinen keine Lautsprecher eingerichtet zu sein.";

"no_wifi.buttons.go_to_settings" = "ZU DEN EINSTELLUNGEN";

"no_wifi.description" = "Es ist keine Wi-Fi-Verbindung auf deinem Gerät verfügbar. Überprüfe es und versuche es erneut.";

"no_wifi.title" = "Entschuldigung.";

"NSLocationWhenInUseUsageDescription" = "Wir versuchen, mithilfe deines Standorts die geeignetsten Sender zu ermitteln, um sie auf deinen Urbanears-Lautsprecher zu speichern.";

"player.aux.activate" = "AKTIVIEREN";

"player.aux.activated" = "AUX AKTIVIERT";

"player.aux.audio_active" = "Audio aktiv";

"player.bluetooth.activate" = "AKTIVIEREN";

"player.bluetooth.activated" = "BLUETOOTH AKTIVIERT";

"player.bluetooth.bluetooth_settings" = "BLUETOOTH-EINSTELLUNGEN";

"player.bluetooth.connected" = "Bluetooth – verbunden";

"player.bluetooth.connected_to" = "Verbunden mit";

"player.bluetooth.enter_pairing" = "KOPPLUNGSMODUS";

"player.bluetooth.idle" = "Idle";

"player.bluetooth.waiting_to_pair" = "WARTEN AUF KOPPLUNG";

"player.cloud.airplay" = "AIRPLAY";

"player.cloud.google_cast" = "INTEGRIERTES CHROMECAST";

"player.cloud.or_play_mobile_app" = "Über Smartphone-App abspielen:";

"player.cloud.play_internet_radio" = "INTERNETRADIO";

"player.cloud.spotify_connect" = "SPOTIFY CONNECT";

"player.connecting.reconnecting" = "Erneut verbinden …";

"player.now_playing.browse_stations" = "NACH SENDERN SUCHEN";

"player.preset.empty_placeholder" = "Voreinstellung leeren";

"player.preset.notification.add" = "Voreinstellung hinzugefügt";

"player.preset.notification.add_playlist" = "Playliste hinzugefügt";

"player.preset.notification.add_radio_station" = "Radiosender hinzugefügt";

"player.preset.notification.addfailed" = "Hinzufügen der Voreinstellung fehlgeschlagen";

"player.preset.notification.addfailedinternetradio" = "Hinzufügen des Radiosenders fehlgeschlagen";

"player.preset.notification.addfailedplaylist" = "Hinzufügen der Playliste fehlgeschlagen";

"player.preset.notification.delete" = "Voreinstellung gelöscht";

"player.preset.notification.delete_internet_radio" = "Radiosender gelöscht";

"player.preset.notification.delete_playlist" = "Playliste gelöscht";

"player.preset.notification.empty" = "Voreinstellung ist leer";

"player.preset.notification.play" = "Abspielen von Voreinstellung";

"player.preset.spotify_login_error.cancel_button" = "Abbrechen";

"player.preset.spotify_login_error.content" = "Anmelden bei Spotify nicht möglich";

"player.preset.spotify_login_error.retry_button" = "Erneut versuchen";

"player.preset.spotify_login_error.title" = "Spotify Fehlermeldung";

"player.preset.spotify_premium_error.content" = "Du hast kein Premium-Konto. Um Spotify auf deinen Lautsprecher zu streamen, ist ein Premium-Konto erforderlich.";

"player.preset.spotify_premium_error.dismiss_button" = "OK";

"player.preset.spotify_premium_error.title" = "Spotify Fehlermeldung";

"player.session_lost.buttons.go_to_home" = "ZUM STARTBILDSCHIRM";

"player.session_lost.buttons.reconnect" = "ERNEUT VERBINDEN";

"player.session_lost.generic_error_to_speaker" = "Ein unbekannter Fehler ist aufgetreten bei der Kommunikation mit %@. Versuche, erneut eine Verbindung herzustellen, durch Drücken auf \"Erneut verbinden\"";

"player.session_lost.network_connection_lost_to_speaker" = "%@ hat die Wi-Fi-Verbindung\nverloren.";

"player.session_lost.speaker_not_connected" = "Lautsprecher ist noch nicht verbunden";

"player.session_lost.timeout" = "Verbindung zu %@ ist abgelaufen. Drücke auf \"Erneut verbinden\", um es erneut zu versuchen.";

"player.session_lost.user_is_controlling_speaker" = "%@ wird von einem anderen Benutzer gesteuert. Übernimm die Kontrolle durch Drücken von \"Erneut verbinden\"";

"presets_disabled.buttons.continue" = "VERSTANDEN";

"presets_disabled.buttons.read_more" = "WEITERLESEN";

"presets_disabled.content" = "Um eine Voreinstellung hinzuzufügen, spiele entweder:\n\n– Musik über Spotify Connect\n– Internetradio (zu finden unter [cloud_image])\n\nWird eine dieser Quellen abgespielt, ist die +-Taste aktiviert. Durch Antippen fügst du die gerade laufende Playliste oder den Radiosender hinzu.";

"presets_disabled.title" = "So fügst du eine Voreinstellung hinzu";

"save_preset.title" = "HINZUFÜGEN EINER VOREINSTELLUNG";

"settings.about.cast_version" = "Versionsnummer derintegrierten Chromecast-Technologie:";

"settings.about.ip" = "IP:";

"settings.about.mac" = "MAC:";

"settings.about.model" = "Modell:";

"settings.about.name" = "Name:";

"settings.about.software_version" = "Software-Version:";

"settings.about.title" = "ÜBER DIESEN LAUTSPRECHER";

"settings.about.wi_fi_network" = "Wi-Fi-Netz:";

"settings.equalizer.bass" = "BASS";

"settings.equalizer.reset_eq" = "ZURÜCKSETZEN";

"settings.equalizer.title" = "EQUALIZER";

"settings.equalizer.treble" = "HÖHEN";

"settings.google_cast.google_cast_enabled_apps" = "Chromecast-fähige Apps";

"settings.google_cast.google_cast_privacy" = "Datenschutzerklärung für die integrierte Chromecast-Technologie";

"settings.google_cast.google_terms" = "Google Nutzungsbedingungen";

"settings.google_cast.learn_how_to_cast" = "Streaming ganz einfach";

"settings.google_cast.open_source" = "Open-Source-Lizenzen";

"settings.google_cast.share_usage_data" = "Nutzungsdaten teilen";

"settings.google_cast.switch_off" = "AUS";

"settings.google_cast.switch_on" = "AN";

"settings.google_cast.title" = "EINSTELLUNGEN DER INTEGRIERTEN CHROMECAST-TECHNOLOGIE";

"settings.rename.title" = "NAMEN ÄNDERN";

"settings.speaker.about" = "Über diesen Lautsprecher";

"settings.speaker.change_name" = "Namen ändern";

"settings.speaker.equalizer" = "Equalizer einstellen";

"settings.speaker.google_privacy" = "Einstellungen der integrierten Chromecast-Technologie";

"settings.speaker.streaming_quality" = "Multi-Streaming-Qualität";

"settings.speaker.time_zone" = "Zeitzone";

"settings.streaming_quality.cellLabel" = "Multi-Streaming-Qualität";

"settings.streaming_quality.description" = "Die Einstellung „Normal“ passt sich automatisch der Qualität des Netzwerksignals an und ist besonders dann geeignet, wenn Probleme auftreten. Diese Einstellung verwendet immer die höchste Qualität. \n\nDie Einstellungen für die Multi-Streaming-Qualität betreffen nur den Multi-Mode.";

"settings.streaming_quality.high" = "Hoch";

"settings.streaming_quality.normal_recommended" = "Normal (Empfohlen)";

"settings.streaming_quality.title" = "MULTI-STREAMING-QUALITÄT";

"settings.timezone.title" = "DATUM UND UHRZEIT";

"setup.connect_spotify.buttons.connect_to_spotify" = "MIT SPOTIFY VERBINDEN";

"setup.connect_spotify.buttons.sign_up" = "ANMELDEN";

"setup.connect_spotify.paragraph1.title" = "Mit Spotify verbinden";

"setup.connect_spotify.paragraph2.content" = "Melde dich jetzt an und erhalte eine kostenlose Testversion von Spotify Premium.";

"setup.connect_spotify.paragraph2.title" = "Neu bei Spotify?";

"setup.done.buttons.done" = "LOS GEHT’S";

"setup.done.content" = "Alles okay.\nEs ist Zeit, deinen Lautsprecher loslegen zu lassen.\nViel Spaß.";

"setup.done.title" = "Alles erledigt";

"setup.failed_boot.message" = "Wenn beide LED-Lichter beständig leuchten, fährt der Lautsprecher hoch. Das dauert etwa 20 Sekunden.";

"setup.failed_setup.buttons.no" = "NEIN";

"setup.failed_setup.buttons.yes" = "JA";

"setup.failed_setup.message" = "Wenn alle LED-Lichter blinken,\nbefindet sich der Lautsprecher im Setup-Modus.\n\nBlinken alle LED-Lichter?";

"setup.list.found_speakers_with_count" = "Okay. %d Lautsprecher wurde(n) gefunden.\nWählen einen aus, um fortzufahren. Du wirst zu einem neuen Bildschirm weitergeleitet. Wähle dein Netzwerk aus und drücke auf \"Weiter\".";

"setup.loading.looking_for_speakers" = "Nach Connected Speakers suchen ...";

"setup.pick_presets.buttons.pick_them_now" = "JETZT VOREINSTELLUNGEN HINZUFÜGEN";

"setup.pick_presets.internet_radio_description" = "Mehr als 30.000 Radiosender weltweit";

"setup.pick_presets.internet_radio_title" = "Internetradio";

"setup.pick_presets.spotify_description" = "Playlisten, Alben, Interpreten, Musikrichtungen & Podcasts";

"setup.pick_presets.title" = "Voreinstellungen";

"setup.presets_list.buttons.move_on" = "WEITER GEHEN";

"setup.presets_list.title" = "Lerne deine Voreinstellungen kennen";

"setup.presets_loading.adding_preset_title" = "Hinzufügen von %@";

"setup.presets_loading.done" = "Fertig";

"setup.presets_loading.title" = "Hinzufügen von Voreinstellungen";

"setup.presets_tutorial.buttons.continue" = "VERSTANDEN";

"setup.presets_tutorial.content_bottom" = "Um eine Voreinstellung zu speichern, dreh den Knopf auf eine Zahl und halte ihn länger gedrückt beim Hören der Musik über Spotify oder Internetradio.";

"setup.presets_tutorial.content_top" = "Um eine Voreinstellung abzuspielen, dreh den Knopf auf eine Zahl und drücke darauf.";

"setup.presets_tutorial.title" = "Kinderleicht";

"setup.rename.buttons.keep_this_name" = "DIESEN NAMEN BEIBEHALTEN";

"setup.rename.buttons.learn_how_it_works" = "SO FUNKTIONIERT ES";

"setup.rename.buttons.rename" = "UMBENENNEN";

"setup.rename.error.empty_name" = "Der Name des Lautsprechers darf nicht leer bleiben";

"setup.rename.error.maximum_length" = "Der Name des Lautsprechers darf nicht länger als %d Zeichen sein";

"setup.rename.error.special_characters" = "Der Name des Lautsprechers darf nicht dieses Sonderzeichen enthalten";

"setup.rename.speaker_is_now_called" = "Der Lautsprecher hat den Namen:\n%@";

"setup.rename.successfully_installed_speaker" = "%@\nist jetzt eingerichtet. Juchhu.";

"setup.spotify_success.title" = "Alles erledigt";

"setup.spotify_success.your_spotify_account" = "Dein Spotify-Konto";

"setup.tutorial.cast.title" = "Integriertes Chromecast";

"setup.tutorial.cloud.airplay" = "AirPlay";

"setup.tutorial.cloud.airplayOpenMusic" = "APPLE MUSIC ÖFFNEN";

"setup.tutorial.cloud.content" = "Spiele Musik von deiner Lieblings-App ab.\n\nHalte nach einem dieser Symbole Ausschau. \nTippe darauf und wähle anschließend deinen Lautsprecher aus \nder Liste.";

"setup.tutorial.cloud.google_cast" = "Integriertes Chromecast";

"setup.tutorial.cloud.spotify_connect" = "Spotify Connect";

"setup.tutorial.cloud.title" = "Cloud Play";

"setup.tutorial.internet_radio.title" = "Internetradio";

"setup.tutorial.multi.title" = "Multi-Mode";

"setup.update.state.checking" = "Nach Updates suchen";

"setup.update.state.downloading" = "Dein Lautsprecher lädt das Update herunter.\n\nLehn dich entspannt zurück, das kann einige Minuten dauern.";

"setup.update.state.finished" = "Dein Lautsprecher ist auf dem neuesten Stand";

"setup.update.state.installing" = "Dein Lautsprecher installiert das Update.\n\nLehn dich entspannt zurück, das kann einige Minuten dauern.";

"setup.update.state.no_update_available" = "Dein Lautsprecher ist auf dem neuesten Stand";

"setup.update.state.restarting" = "Dein Lautsprecher macht einen Neustart";

"setup.update.text" = "Dein Lautsprecher wird aktualisiert.\nLehn dich entspannt zurück, das kann\neinige Minuten dauern.";

"spotify_source.buttons.get_spotify" = "SPOTIFY ERHALTEN";

"spotify_source.buttons.open_spotify" = "SPOTIFY ÖFFNEN";

"spotify_source.enjoy" = "Genieße die Beats.";

"spotify_source.instructions" = "1. \tÖffne die Spotify-App auf deinem Smartphone, Tablet oder Laptop.\n2. \tSpiele einen Song und wähle Verfügbare Geräte.\n3.\tWähle das Gerät aus und beginne, Musik zu hören.";

"spotify_source.intro" = "Bereit, Musik abzuspielen?\nHöre Spotify auf %@ und nutze die Spotify-App als Fernbedienung.";

"terms.buttons.accept" = "ANNEHMEN & WEITER";

"terms.text.google_terms.content" = "In der Datenschutzerklärung von Google wird erläutert, welche Informationen Google während der Einrichtung und Nutzung von Chromecast erhebt. Du kannst die Datenschutzeinstellungen der integrierten Chromecast-Technologie nach der Einrichtung deines Geräts in den Geräteeinstellungen dieser App ändern und z. B. festlegen, ob Nutzungsdaten und Absturzberichte an Google gesendet werden sollen.\n\nDurch Auswahl von \"Akzeptieren\" bestätigst du, dass du den [terms_link] zustimmst und die [privacy_link] gelesen hast.";

"terms.text.google_terms.privacy_link.link" = "https://www.google.com/policies/privacy/";

"terms.text.google_terms.privacy_link.text" = "Google Datenschutzerklärung";

"terms.text.google_terms.terms_link.link" = "https://www.google.com/policies/terms/";

"terms.text.google_terms.terms_link.text" = "Google Datenschutzerklärung";

"terms.text.ue_terms.content" = "Durch Nutzung dieser App stimmst du zu, an den Endbenutzer-Lizenzvertrag von Urbanears Connected gebunden zu sein, der die Rechte und Pflichten bezüglich der App und deiner Connected Speakers von Urbanears regelt.\n\nDurch Tippen auf \"Annehmen & Weiter\" unten, bestätigst du, dass du dem [ue_eula_link] zustimmst";

"terms.text.ue_terms.terms_link.link" = "https://www.urbanears.com/ue_gb_en/";

"terms.text.ue_terms.terms_link.text" = "Endbenutzer-Lizenzvertrag";

"terms.title" = "ALLGEMEINE GESCHÄFTSBEDINGUNGEN";

"volume.headers.master_volume_label" = "MULTI";

"volume.title" = "LAUTSTÄRKE";

"welcome.description.text" = "Tippe oben, um den Allgemeinen [terms_link], den [google_terms_link] und der [google_privacy_link] von Google zuzustimmen, und du kannst loslegen.";

"welcome.title" = "Hallo.\nLos geht’s";

"about.eula.website" = "https://www.urbanears.com/urbanears_connected_speaker_eula_de";

"about.privacy_policy.website" = "https://www.urbanears.com/app-privacy-policy-de";

"welcome.description.google_terms_text" = "Nutzungsbedingungen";

"welcome.description.google_privacy_text" = "Datenschutzrichtlinie";

"welcome.description.terms_link_text" = "Geschäftsbedingungen";

"welcome.buttons.accept" = "STARTEN";

"setup.presets_fail.title" = "Entschuldigung";

"setup.presets_fail.buttons.try_again" = "ERNEUT VERSUCHEN";

"help.contact.website" = "http://www.urbanears.com";

"help.contact.email" = "support@urbanears.com";

"help.online_manual.website" = "http://www.urbanears.com/speaker-support";

"about.foss.website" = "https://www.urbanears.com/ue_us_en/urbanears_connected_speaker_foss";

"volume.role.multi" = "MULTI";

"volume.role.solo" = "SOLO";

"player.aux.mode_name" = "AUX";

"setup.update.state.update_failed" = "Update auf Lautsprecher fehlgeschlagen.";

"player.bluetooth.connect_device_advice" = "Stelle sicher, dass Bluetooth auf deinem %@ aktiviert ist.";

"settings.timezone.search_hint" = "Zeitzone suchen";

"setup.presets_loading.preparing_presets" = "Abrufen von Voreinstellungen";

"player.preset.login_spotify_advice" = "Hier tippen, um eine Verbindung mit Spotify herzustellen. Das voreingestellte Werk wird aktualisiert.";

"terms.text.google_terms.title" = "Google Nutzungsbedingungen und Google Datenschutzerklärung";

"terms.text.ue_terms.title" = "Urbanears Connected Endbenutzer-Lizenzvertrag";

"player.cloud.internet_radio_mode_name" = "Internetradio";

"player.cloud.google_cast_mode_name" = "Intergriertes Chromecast";

"player.preset.spotify_playlist" = "Spotify Playlist";

"player.carousel.playable_source.aux" = "AUX";

"player.carousel.playable_source.preset" = "Voreinstellung";

"player.carousel.playable_source.bluetooth" = "Bluetooth";

"player.carousel.playable_source.cloud" = "CLOUD";

"setup.troubleshooting.title" = "Problemlösung";

"setup.troubleshooting.content1" = "Versuchen Sie diese Schritte, um sicherzustellen, dass Ihre Lautsprecher sind so gut, Wi-Fi-Verbindung wie möglich:";

"setup.troubleshooting.content2.item1" = "Überprüfen Sie, ob Ihr WLAN funktioniert";

"setup.troubleshooting.content2.item2" = "Starten Sie Ihren WLAN-Router";

"setup.troubleshooting.content2.item3" = "Achten Sie darauf, nicht blockiert den Signalweg";

"setup.troubleshooting.content2.item4" = "Bewegen Sie die Lautsprecher an den Router näher";

"setup.troubleshooting.content2.item5" = "Starten Sie Ihre Lautsprecher";

"setup.troubleshooting.content3" = "Ist es immer noch? Kontaktieren Sie Ihre Unterstützung, wir helfen Ihnen gerne weiter!";

"setup.troubleshooting.buttons.restart_setup" = "NEUSTART CONFIGURATION";

"setup.troubleshooting.buttons.contact_support" = "SUPPORT KONTAKTIEREN";

"setup.configuring.configuring_speaker" = "Konfigurieren von %@";

"setup.configuring.searching_for_speaker" = "Suche nach %@";

"setup.configuring.found_speaker" = "Gefunden %@";

"setup.lost_connection.title" = "Verbindung getrennt";

"setup.lost_connection.buttons.reconnect" = "ERNEUT VERBINDEN";

"setup.lost_connection.buttons.troubleshooting" = "Problemlösung";

"setup.wac_failure.title" = "Entschuldigung";

"setup.wac_failure.content" = "Es scheint, dass etwas schief gelaufen ist. Schließen Sie die App und öffnen Sie es.";

"setup.wac_failure.buttons.close_app" = "Schließen Sie die App";

"help.contact.support_website" = "http://www.urbanears.com/speaker-contact";

"settings.root_menu.solo_speakers" = "Solo Speakers";

"settings.multi_menu.multi_streaming_quality" = "Multi-Streaming-Qualität";

"settings.multi_menu.stereo_pairing" = "Stereo Paring";

"setup.pick_presets.content" = "Auf diesem Lautsprecher kannst du deine Lieblingsmusik von Spotify und aus dem Internetradio als sieben einzelne Voreinstellungen abspeichern.\n\n Sollen wir für dich Voreinstellungen auf deinem Lautsprecher hinzufügen, damit du gleich starten kannst? Du kannst sie später ändern.";

"setup.failed_connect.message" = "Stelle sicher, dass dein Lautsprecher an das Stromnetz angeschlossen ist.";

"setup.failed_boot.message" = "Wenn beide LED-Lichter beständig leuchten, fährt der Lautsprecher hoch. Das dauert etwa 20 Sekunden.";

"setup.failed_connect.title" = "SETUP-ANLEITUNG";

"setup.failed_reset.message" = "Dreh den Knopf zur Cloud und halte ihn 5 Sekunden gedrückt, bis die Lichter anfangen zu blinken, um den Lautsprecher in den Setup-Modus zu bringen.";

"setup.connect_spotify.paragraph1.content" = "Es ist leicht, Spotify auf deinem Lautsprecher abzuspielen.\n\nSobald du verbunden bist, öffne die Spotify-App, um kabellos Musik abzuspielen und zu steuern.";

"setup.spotify_success.is_linked" = "ist jetzt mit deinem Lautsprecher verlinkt.\n\nZeit, mit den Voreinstellungen loszulegen.";

"setup.lost_connection.content" = "Es scheint, als ob die Verbindung mit dem Lautsprecher während der Netzwerkeinstellungen verloren. Versuchen Sie erneut zu verbinden oder die Anleitung zur Fehlerbehebung, um Hilfe zu lesen.";

"player.now_playing.save_preset" = "ZUR VOREINSTELLUNG HINZUFÜGEN";

"setup.tutorial.internet_radio.content" = "Durchsuche und spiele mehr als 30.000 Internet-Radiosender über diese App ab.\n\nDu findest das Internetradio unter der Cloud in der App.";

"setup.tutorial.multi.content" = "Du kannst Gruppen mit mehreren Connected Speakers für ein synchronisiertes Playdate bilden.\n\nSetze einen Lautsprecher auf Multi-Mode über diese App oder durch Drücken des Lautstärkereglers auf dem oberen Panel des Lautsprechers.";

"setup.tutorial.cast.content_top" = "Mit integriertem Chromecast kannst du Musik, Internetradio oder Podcasts deiner Lieblings-Apps auf deine Lautsprecher streamen.";

"volume.buttons.mute_all" = "TON AUS";

"volume.buttons.unmute_all" = "TON AN";

"help.online_manual_content" = "Handbücher sind in mehreren Sprachen auf der Urbanears-Website erhältlich.";

"setup.could_not_find_speaker" = "Es wurde kein Lautsprecher im ausgewählten Netzwerk gefunden.\n\nEs kann sein, dass der Lautsprecher länger als erwartet für die Einrichtung benötigt. Wähle \"Zum Startbildschirm\", um zu sehen, ob der Lautsprecher dort aufgeführt wird.\n\nWird er nicht aufgeführt, greife auf das Menü (in der oberen linken Ecke) zu und wähle \"Neuen Lautsprecher einrichten\" und versuche es erneut.";

"spotify_error_title" = "Etwas ist fehlgeschlagen";

"setup.presets_fail.content" = "Beim Hinzufügen der Voreinstellungen auf deinem Lautsprecher scheint es ein Problem zu geben. Bitte versuche es erneut oder gehe zurück. Du kannst die Voreinstellungen auch später hinzufügen.";

"undo_succeeded" = "Rückgängig machen erfolgreich";

"undo_failed" = "Rückgängig machen fehlgeschlagen";

"updateavailable_module.content" = "%@ verwendet nicht das neueste Update für Connected Speakers. \n\nDas Update kann in den Lautsprechereinstellungen über das Menü \„Über diesen Lautsprecher\“ manuell durchgeführt werden. Alternativ findet das Update über Nacht statt.";

"settings.about.update.content" = "%@ wurde zum Update ausgewählt. \n\nWährend des Updates kann der Connected Speaker vorübergehend nicht verwendet werden. Sobald das Update abgeschlossen ist, erscheint der Lautsprecher wieder auf dem Home Screen.";

"settings.about.update.buttonLabel" = "LAUTSPRECHER AKTUALISIEREN";
